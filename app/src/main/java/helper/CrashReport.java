package helper;

import android.app.Application;
import android.provider.Settings;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

import java.util.LinkedHashMap;

public class CrashReport extends Application {
    public static CrashReport global;
    public static LinkedHashMap<String, String> crashData;

    @Override
    public void onCreate() {
        super.onCreate();
        global = this;

        crashData = new LinkedHashMap<>();

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable throwable) {
                FirebaseCrash.report(throwable);
            }
        });
    }

    public static CrashReport getInstance() {

        return global;
    }

    public static void sendReport(Exception e)
    {
        FirebaseCrash.logcat(Log.ERROR, "DATA", crashData.toString());
        FirebaseCrash.report(e);
    }
}
