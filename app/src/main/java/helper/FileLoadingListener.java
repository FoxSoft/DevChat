package helper;

public interface FileLoadingListener {
    void onBegin();

    void onSuccess();

    void onFailure(Throwable cause);

    void onEnd();
}
