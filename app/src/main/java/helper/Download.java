package helper;

import android.os.AsyncTask;

import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.xml.parsers.DocumentBuilderFactory;

public class Download extends AsyncTask<String, Void, Void> {
    private String url = "";
    private File destination;
    private FileLoadingListener fileLoadingListener;
    private Throwable throwable;

    public Download(String url, File destination, FileLoadingListener fileLoadingListener) {
        this.url = url;
        this.destination = destination;
        this.fileLoadingListener = fileLoadingListener;
    }

    @Override
    protected Void doInBackground(String... strings) {
        try {
            FileUtils.copyURLToFile(new URL(url), destination);
        } catch (IOException e) {
            throwable = e;
        }
        return null;
    }

    protected void onPostExecute(Void aVoid) {

        fileLoadingListener.onEnd();
        if (throwable != null) {
            fileLoadingListener.onFailure(throwable);
        } else {
            fileLoadingListener.onSuccess();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        fileLoadingListener.onBegin();
    }

    private String getContent(String path) throws IOException {
        BufferedReader reader = null;
        try {
            URL url = new URL(path);
            HttpsURLConnection c = (HttpsURLConnection) url.openConnection();
            c.setRequestMethod("GET");
            c.setReadTimeout(10000);
            c.connect();
            reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
            StringBuilder buf = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                buf.append(line + "\n");
            }
            return (buf.toString());
        } finally {
            if (reader != null) {
                reader.close();
            }
        }

    }
}
