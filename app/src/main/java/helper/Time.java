package helper;

import android.app.Application;
import android.content.Context;

public class Time extends Application {
    private static final int SECOND_MILIS = 1000;
    private static final int MINUTE_MILIS = SECOND_MILIS * 60;
    private static final int HOUR_MILIS = MINUTE_MILIS * 60;
    private static final int DAY_MILIS = HOUR_MILIS * 24;

    public static String getTimeAgo(long time, Context context)
    {
        if (time < 1000000000000L){
            time *= 100;
        }

        long now = System.currentTimeMillis();
        if (time > now || time <= 0){
            return "вышел(а) тлько что";
        }

        final long diff = now - time;
        if (diff < MINUTE_MILIS){
            return "вышел(а) тлько что";
        } else if (diff < 2 * MINUTE_MILIS){
            return "минуту назад";
        } else if (diff < 50 * MINUTE_MILIS){
            return diff/MINUTE_MILIS + " мин. назад";
        } else if (diff < 90 * MINUTE_MILIS){
            return "час назад";
        } else if (diff < 24 * HOUR_MILIS){
            return diff/HOUR_MILIS + " ч. назад";
        } else if (diff < 48 * HOUR_MILIS){
            return "вчера";
        } else return diff/DAY_MILIS + " дней назад";
    }
}
