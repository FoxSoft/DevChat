package helper;

import android.graphics.Color;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.github.chrisbanes.photoview.PhotoView;
import com.github.chrisbanes.photoview.PhotoViewAttacher;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrInterface;
import com.r0adkll.slidr.model.SlidrListener;
import com.r0adkll.slidr.model.SlidrPosition;
import com.squareup.picasso.Picasso;

import progcha.nosenko.com.R;

import static android.support.constraint.ConstraintLayout.LayoutParams.BOTTOM;
import static android.support.constraint.ConstraintLayout.LayoutParams.TOP;
import static android.support.v7.widget.helper.ItemTouchHelper.RIGHT;
import static android.widget.LinearLayout.HORIZONTAL;
import static android.widget.LinearLayout.VERTICAL;

/**
 * Created by eminem on 22.11.2017.
 */

public class FullScreenImage extends AppCompatActivity {
    private PhotoView photoView;
    private SlidrInterface slidrInterface;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.img_fullscreen);
        photoView = (PhotoView) findViewById(R.id.photo_view);
        PhotoViewAttacher attacher = new PhotoViewAttacher(photoView);
        attacher.setZoomable(true);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String link = extras.getString("link");
            Picasso.get().load(link).placeholder(R.drawable.default_avatar).into(photoView);
            Log.d("ID", link);
        }

//        SlidrConfig config = new SlidrConfig.Builder().primaryColor(getResources().getColor(R.color.green))
//                .position(SlidrPosition.BOTTOM).position(SlidrPosition.TOP).position(SlidrPosition.RIGHT).position(SlidrPosition.LEFT)
//                .sensitivity(1f)
//                .scrimColor(Color.BLACK)
//                .scrimStartAlpha(0.8f)
//                .scrimEndAlpha(0f)
//                .velocityThreshold(2400)
//                .distanceThreshold(0.25f)
//                .edge(true | false)
//                .edgeSize(0.18f) // The % of the screen that counts as the edge, default 18%
//                .build();
////
////        Slidr.attach(this, config);

        slidrInterface = Slidr.replace(photoView, new SlidrConfig.Builder().position(SlidrPosition.LEFT)
                .position(SlidrPosition.TOP).position(SlidrPosition.RIGHT).position(SlidrPosition.BOTTOM)
                .scrimColor(getResources().getColor(R.color.prozColor)).secondaryColor(getResources().getColor(R.color.prozColor))
                .primaryColor(getResources().getColor(R.color.prozColor))
                .build());
    }

}
