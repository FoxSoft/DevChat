package helper;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nex3z.notificationbadge.NotificationBadge;

import progcha.nosenko.com.R;

public class HelperTabTitle {
    private NotificationBadge badge;
    private int click = 0;
    private ImageView img;
    private int number;
    private RelativeLayout relativeLayout;
    private TextView title;
    private View view;

    public HelperTabTitle(View v) {
        view = v;
        badge = view.findViewById(R.id.badge);
        title = view.findViewById(R.id.nameStation);
        img = view.findViewById(R.id.icon);
        relativeLayout = view.findViewById(R.id.RL);
    }

    public void setNumberBadge(int n) {
        badge.setNumber(n);
        number = n;
    }

    public void incNumerBadge() {
        NotificationBadge notificationBadge = badge;
        int i = number + 1;
        number = i;
        notificationBadge.setNumber(i);
    }

    public void setImg(int idImg) {
        img.setImageResource(idImg);
    }

    public void setTitle(String s) {
        title.setText(s);
    }
}