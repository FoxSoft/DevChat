package helper;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.sdsmdg.tastytoast.TastyToast;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import model.Chat;
import model.Notifications;
import model.Users;
import progcha.nosenko.com.R;

public class Helper {
    private static FirebaseUser currentUser;

    public static enum DISPLAY {
        width, height;
    }

    public static void setLastSeen() {

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference last = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid()).child("lastSeen");
        last.setValue(ServerValue.TIMESTAMP).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                }
            }
        });
    }

    public static void setStatusNet(StatusUser.StatusNet stat) {
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference statusNet = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid()).child("statusNet");
        statusNet.setValue(stat).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                }
            }
        });
    }

    public static int getSizeDisplay(WindowManager manager, DISPLAY param) {
        Display display = manager.getDefaultDisplay();
        DisplayMetrics metricsB = new DisplayMetrics();
        display.getRealMetrics(metricsB);
        switch (param) {
            case width:
                return metricsB.widthPixels;
            case height:
                return metricsB.heightPixels;
            default:
                return 0;
        }
    }

    public static String getTimeNow() {
        Calendar calendar = Calendar.getInstance();
        return "" + calendar.getTime().getHours() + " ч. " + calendar.getTime().getMinutes() + " мин.";
    }

    public static void pushMessageNotification(Notifications notifications) {
        DatabaseReference notifi = FirebaseDatabase.getInstance().getReference().child("notifications");
        notifi.child(notifications.getTo()).push().setValue(notifications).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        });
    }

    public static boolean isOnline3(Context context) {
        String cs = Context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(cs);
        if (cm.getActiveNetworkInfo() == null) {
            return false;
        } else {
            return true;
        }
    }

    public static boolean hashFile(String patch) {
        File f = new File(Environment.getExternalStorageDirectory() + patch);
        if (f.exists()) // файл есть
            return true;
        else
            return false;
    }

    public static String decodeNameFile(String nameFile) {
        String result = "_::_";

        if (nameFile.contains(result))
            result = nameFile.substring(nameFile.indexOf(result) + result.length());

        if (nameFile.contains("_%3A%3A_")) {
            result = "_%3A%3A_";
            result = nameFile.substring(nameFile.indexOf(result) + result.length());
            result = result.substring(0, result.indexOf(".mp3") + 4);
        }

        if (result.contains("%20")) {
            result = result.replace("%20", " ");
        }

        if (!result.isEmpty()) {
            return result;
        } else

            return nameFile;
    }

    public static String createTimeFinish(int time) {
        String result = "";

        int min = time / 1000 / 60;
        int sec = time / 1000 % 60;

        result = min + " : ";

        if (sec < 10)
            result += "0" + sec;
        result += sec;

        return result;
    }

    public static String getFileName(Uri uri, Context context) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

}
