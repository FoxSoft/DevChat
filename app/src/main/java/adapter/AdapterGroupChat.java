package adapter;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import com.xw.repo.BubbleSeekBar;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import helper.Download;
import helper.FileLoadingListener;
import helper.FullScreenImage;
import helper.Helper;
import model.Chat;
import progcha.nosenko.com.R;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class AdapterGroupChat extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<Chat> chats;
    private Context context;
    private String thumbFriend, thumbUser;
    private FirebaseUser currentUser;
    private DatabaseReference refUser;
    private Boolean statusSeenFriend = false;
    private MediaPlayer mediaPlayer;
    private int idPlay;
    private String nameMusic;
    private Map usersThumb;

    public void setStatusSeenFriend(Boolean statusSeenFriend) {
        this.statusSeenFriend = statusSeenFriend;
    }

    public Boolean getStatusSeenFriend() {
        return statusSeenFriend;
    }

    private enum TYPE_VIEW {
        user_text(0), user_picture(1), friend_text(2), friend_picture(3), user_audio(4), friend_audio(5);

        private int type;

        TYPE_VIEW(int i) {
            this.type = i;
        }

        public int getType() {
            return type;
        }
    }

    public AdapterGroupChat(final List<Chat> chats, Context context, Map thumbs) {
        this.chats = chats;
        this.context = context;
        this.currentUser = FirebaseAuth.getInstance().getCurrentUser();
        this.refUser = FirebaseDatabase.getInstance().getReference().child("users");
        this.usersThumb = thumbs;
        this.refUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                thumbUser = dataSnapshot.child(currentUser.getUid()).child("thumb").getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        RecyclerView.ViewHolder holder = null;

        switch (viewType) {
            case 0:
                view = LayoutInflater.from(context).inflate(R.layout.item_chat_user_text, parent, false);
                holder = new ViewHolderChatUserT(view);
                break;

            case 1:
                view = LayoutInflater.from(context).inflate(R.layout.item_chat_user_img, parent, false);
                holder = new ViewHolderChatUserP(view);
                break;

            case 2:
                view = LayoutInflater.from(context).inflate(R.layout.item_chat_friend_text, parent, false);
                holder = new ViewHolderChatFriendT(view);
                break;

            case 3:
                view = LayoutInflater.from(context).inflate(R.layout.item_chat_friend_img, parent, false);
                holder = new ViewHolderChatFriendP(view);
                break;

            case 4:
                view = LayoutInflater.from(context).inflate(R.layout.item_chat_user_music, parent, false);
                holder = new ViewHolderChatUserM(view);
                break;

            case 5:
                view = LayoutInflater.from(context).inflate(R.layout.item_chat_friend_music, parent, false);
                holder = new ViewHolderChatFriendM(view);
                break;
        }
        return holder;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void updateSeeItem(boolean isSee) {

        if (isSee) {
            setStatusSeenFriend(true);
            for (int i = 0; i <= getItemCount() - 1; i++)
                if (!chats.get(i).isSeen()) {
                    Log.d("updateSeeItem", " position: " + i);
                    chats.get(i).setSeen(true);
                    notifyItemChanged(i);
                }
        }
//        int at = -1;
//        if (getStatusSeenFriend()) {
//            for (int i = 0; i <= getItemCount()-1; i++)
//                if (!chats.get(i).isSeen()) {
//                    Log.d("updateSeeItem", " position: " + i);
//                    chats.get(i).setSeen(true);
//                    if (at < 0)
//                        at = i;
//
//                }
//            notifyItemRangeChanged(at, 5);
//        }

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        final Chat chat = chats.get(position);
        switch (getItemViewType(position)) {
            case 0:
                final ViewHolderChatUserT viewHolderChatUserT = (ViewHolderChatUserT) holder;

                viewHolderChatUserT.message.setText(chat.getMessage());

                viewHolderChatUserT.date.setText(chat.getTime());

                try {
                    if (!thumbUser.isEmpty())
                        Picasso.get().load(thumbUser).placeholder(R.drawable.default_avatar).into(viewHolderChatUserT.logo);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (chat.isSeen())
                    viewHolderChatUserT.seeImg.setAlpha(1f);
                else
                    viewHolderChatUserT.seeImg.setAlpha(0.5f);

                break;
            case 1:
                ViewHolderChatUserP viewHolderChatUserP = (ViewHolderChatUserP) holder;
                viewHolderChatUserP.img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context.getApplicationContext(), FullScreenImage.class);
                        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("link", chat.getMessage());
                        context.startActivity(intent);
                    }
                });
                Picasso.get().load(chat.getMessage()).placeholder(R.drawable.default_avatar).into(viewHolderChatUserP.img);
                viewHolderChatUserP.date.setText(chat.getTime());

                if (!thumbUser.isEmpty())
                    Picasso.get().load(thumbUser).placeholder(R.drawable.default_avatar).into(viewHolderChatUserP.logo);
                break;
            case 2:
                ViewHolderChatFriendT viewHolderChatFriendT = (ViewHolderChatFriendT) holder;
                viewHolderChatFriendT.message.setText(chat.getMessage());
                viewHolderChatFriendT.date.setText(chat.getTime());

                try {
                    Picasso.get().load(usersThumb.get(chat.getFrom()).toString()).placeholder(R.drawable.default_avatar).into(viewHolderChatFriendT.logo);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case 3:
                ViewHolderChatFriendP viewHolderChatFriendP = (ViewHolderChatFriendP) holder;
                viewHolderChatFriendP.img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context.getApplicationContext(), FullScreenImage.class);
                        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("link", chat.getMessage());
                        context.startActivity(intent);
                    }
                });
                Picasso.get().load(chat.getMessage()).placeholder(R.drawable.default_avatar).into(viewHolderChatFriendP.img);
                viewHolderChatFriendP.date.setText(chat.getTime());


                Picasso.get().load(usersThumb.get(chat.getFrom()).toString()).placeholder(R.drawable.default_avatar).into(viewHolderChatFriendP.logo);
                break;

            case 4:
                final ViewHolderChatUserM viewHolderChatUserM = (ViewHolderChatUserM) holder;
                viewHolderChatUserM.nameMusic.setText(Helper.decodeNameFile(chat.getMessage()));

                nameMusic = Helper.decodeNameFile(chat.getMessage());

                if (!Helper.hashFile("/DevChat/Download/Music/" + nameMusic)) {
                    viewHolderChatUserM.but.setImageResource(R.drawable.download);
                }

                if (chat.isSeen())
                    viewHolderChatUserM.seenImg.setAlpha(1f);
                else
                    viewHolderChatUserM.seenImg.setAlpha(0.5f);

                if (!thumbUser.isEmpty())
                    Picasso.get().load(thumbUser).placeholder(R.drawable.default_avatar).into(viewHolderChatUserM.logo);

                viewHolderChatUserM.but.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        if (!Helper.hashFile("/DevChat/Download/Music/" + nameMusic)) {
                            viewHolderChatUserM.but.setImageResource(R.drawable.download);
                            new Download(chat.getMessage(), new File(Environment.getExternalStorageDirectory() + "/DevChat/Download/Music/" + nameMusic),
                                    new FileLoadingListener() {
                                        @Override
                                        public void onBegin() {
                                            Log.d("download", "begin");
                                        }

                                        @Override
                                        public void onSuccess() {
                                            Log.d("download", "onSuccess");
                                            viewHolderChatUserM.but.setImageResource(R.drawable.play);
                                        }

                                        @Override
                                        public void onFailure(Throwable cause) {
                                            Log.d("download", "onFailure");
                                        }

                                        @Override
                                        public void onEnd() {
                                            Log.d("download", "onEnd");
                                        }
                                    }).execute();
                        } else {
                            viewHolderChatUserM.but.setImageResource(R.drawable.play);

                            if (mediaPlayer == null) {
                                mediaPlayer = new MediaPlayer();
                                try {
                                    mediaPlayer.setDataSource(Environment.getExternalStorageDirectory() + "/DevChat/Download/Music/" + nameMusic);
                                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                        @Override
                                        public void onPrepared(final MediaPlayer mediaPlayer) {
                                            mediaPlayer.start();
                                            idPlay = position;
                                            viewHolderChatUserM.but.setImageResource(R.drawable.pause);
                                            viewHolderChatUserM.seekBar.getConfigBuilder().max(mediaPlayer.getDuration()).build();
                                            viewHolderChatUserM.finish.setText(Helper.createTimeFinish(mediaPlayer.getDuration()));

                                            viewHolderChatUserM.seekBar.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
                                                @Override
                                                public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
                                                    mediaPlayer.seekTo(progress);
                                                }

                                                @Override
                                                public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
                                                    viewHolderChatUserM.seekBar.setProgress(progress);
                                                }

                                                @Override
                                                public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

                                                }

                                            });

                                        }
                                    });
                                    mediaPlayer.prepareAsync();

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            }


                            if (idPlay == position) {

                                if (mediaPlayer.isPlaying()) {
                                    viewHolderChatUserM.but.setImageResource(R.drawable.play);
                                    mediaPlayer.pause();
                                } else {
                                    viewHolderChatUserM.but.setImageResource(R.drawable.pause);
                                    mediaPlayer.start();
                                }
                            } else {
                                try {
                                    mediaPlayer.stop();

                                    notifyItemChanged(idPlay);

                                    Log.d("nextPlay", nameMusic);

                                    mediaPlayer.reset();
                                    mediaPlayer.setDataSource(Environment.getExternalStorageDirectory() + "/DevChat/Download/Music/" + nameMusic);

                                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                        @Override
                                        public void onPrepared(final MediaPlayer mediaPlayer) {
                                            mediaPlayer.start();
                                            idPlay = position;
                                            viewHolderChatUserM.but.setImageResource(R.drawable.pause);
                                            viewHolderChatUserM.seekBar.getConfigBuilder().max(mediaPlayer.getDuration()).build();
                                            viewHolderChatUserM.finish.setText(Helper.createTimeFinish(mediaPlayer.getDuration()));

                                            viewHolderChatUserM.seekBar.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
                                                @Override
                                                public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
                                                    mediaPlayer.seekTo(progress);
                                                }

                                                @Override
                                                public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
                                                    viewHolderChatUserM.seekBar.setProgress(progress);
                                                }

                                                @Override
                                                public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

                                                }

                                            });

                                        }
                                    });
                                    mediaPlayer.prepareAsync();

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                    }
                });
                break;

            case 5:
                final ViewHolderChatFriendM viewHolderChatFriendM = (ViewHolderChatFriendM) holder;
                viewHolderChatFriendM.nameMusic.setText(Helper.decodeNameFile(chat.getMessage()));


                Picasso.get().load(usersThumb.get(chat.getFrom()).toString()).placeholder(R.drawable.default_avatar).into(viewHolderChatFriendM.logo);

                final String nameMusic = Helper.decodeNameFile(chat.getMessage());

                if (!Helper.hashFile("/DevChat/Download/Music/" + nameMusic)) {
                    viewHolderChatFriendM.but.setImageResource(R.drawable.download);
                }
                viewHolderChatFriendM.but.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {


                        if (!Helper.hashFile("/DevChat/Download/Music/" + nameMusic)) {
                            viewHolderChatFriendM.but.setImageResource(R.drawable.download);
                            new Download(chat.getMessage(), new File(Environment.getExternalStorageDirectory() + "/DevChat/Download/Music/" + nameMusic),
                                    new FileLoadingListener() {
                                        @Override
                                        public void onBegin() {
                                            Log.d("download", "begin");
                                        }

                                        @Override
                                        public void onSuccess() {
                                            Log.d("download", "onSuccess");
                                            viewHolderChatFriendM.but.setImageResource(R.drawable.play);
                                        }

                                        @Override
                                        public void onFailure(Throwable cause) {
                                            Log.d("download", "onFailure");
                                        }

                                        @Override
                                        public void onEnd() {
                                            Log.d("download", "onEnd");
                                        }
                                    }).execute();
                        } else {
                            viewHolderChatFriendM.but.setImageResource(R.drawable.play);

                            if (mediaPlayer == null) {
                                mediaPlayer = new MediaPlayer();
                                try {
                                    mediaPlayer.setDataSource(Environment.getExternalStorageDirectory() + "/DevChat/Download/Music/" + nameMusic);
                                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                        @Override
                                        public void onPrepared(final MediaPlayer mediaPlayer) {
                                            mediaPlayer.start();
                                            idPlay = position;
                                            viewHolderChatFriendM.but.setImageResource(R.drawable.pause);
                                            viewHolderChatFriendM.seekBar.getConfigBuilder().max(mediaPlayer.getDuration()).build();
                                            viewHolderChatFriendM.finish.setText(Helper.createTimeFinish(mediaPlayer.getDuration()));

                                            viewHolderChatFriendM.seekBar.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
                                                @Override
                                                public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
                                                    mediaPlayer.seekTo(progress);
                                                }

                                                @Override
                                                public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
                                                    viewHolderChatFriendM.seekBar.setProgress(progress);
                                                }

                                                @Override
                                                public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

                                                }

                                            });

                                        }
                                    });
                                    mediaPlayer.prepareAsync();

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                            }


                            if (idPlay == position) {

                                if (mediaPlayer.isPlaying()) {
                                    viewHolderChatFriendM.but.setImageResource(R.drawable.play);
                                    mediaPlayer.pause();
                                } else {
                                    viewHolderChatFriendM.but.setImageResource(R.drawable.pause);
                                    mediaPlayer.start();
                                }
                            } else {
                                try {
                                    mediaPlayer.stop();

                                    notifyItemChanged(idPlay);

                                    Log.d("nextPlay", nameMusic);

                                    mediaPlayer.reset();
                                    mediaPlayer.setDataSource(Environment.getExternalStorageDirectory() + "/DevChat/Download/Music/" + nameMusic);

                                    mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                        @Override
                                        public void onPrepared(final MediaPlayer mediaPlayer) {
                                            mediaPlayer.start();
                                            idPlay = position;
                                            viewHolderChatFriendM.but.setImageResource(R.drawable.pause);
                                            viewHolderChatFriendM.seekBar.getConfigBuilder().max(mediaPlayer.getDuration()).build();
                                            viewHolderChatFriendM.finish.setText(Helper.createTimeFinish(mediaPlayer.getDuration()));
                                            viewHolderChatFriendM.seekBar.setOnProgressChangedListener(new BubbleSeekBar.OnProgressChangedListener() {
                                                @Override
                                                public void onProgressChanged(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
                                                    mediaPlayer.seekTo(progress);
                                                }

                                                @Override
                                                public void getProgressOnActionUp(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {
                                                    viewHolderChatFriendM.seekBar.setProgress(progress);
                                                }

                                                @Override
                                                public void getProgressOnFinally(BubbleSeekBar bubbleSeekBar, int progress, float progressFloat) {

                                                }

                                            });

                                        }
                                    });
                                    mediaPlayer.prepareAsync();

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                    }
                });
                break;

        }
    }

    public void destoryPlayer() {
        try {
            mediaPlayer.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return chats.size();
    }

    @Override
    public int getItemViewType(int position) {
        int i = 0;

        if (chats.get(position).getFrom().contains(currentUser.getUid())) {
            switch (chats.get(position).getType()) {
                case picture:
                    i = TYPE_VIEW.user_picture.getType();
                    break;
                case text:
                    i = TYPE_VIEW.user_text.getType();
                    break;
                case audio:
                    i = TYPE_VIEW.user_audio.getType();
                    break;
            }
        } else {
            switch (chats.get(position).getType()) {
                case picture:
                    i = TYPE_VIEW.friend_picture.getType();
                    break;
                case text:
                    i = TYPE_VIEW.friend_text.getType();
                    break;

                case audio:
                    i = TYPE_VIEW.friend_audio.getType();
                    break;
            }
        }
        return i;
    }


    public class ViewHolderChatFriendT extends RecyclerView.ViewHolder {
        private ImageView logo;
        private TextView message, date;
        private LinearLayout linearLayout;

        public ViewHolderChatFriendT(View itemView) {
            super(itemView);
            logo = itemView.findViewById(R.id.logo);
            message = itemView.findViewById(R.id.message);
            date = itemView.findViewById(R.id.date);
            linearLayout = itemView.findViewById(R.id.LL);
        }
    }

    public class ViewHolderChatFriendP extends RecyclerView.ViewHolder {
        private ImageView logo, img;
        private TextView date;
        private LinearLayout linearLayout;

        public ViewHolderChatFriendP(View itemView) {
            super(itemView);
            logo = itemView.findViewById(R.id.logo);
            img = itemView.findViewById(R.id.messageImg);
            img.setLayoutParams(new LinearLayout.LayoutParams(400, 400));
            date = itemView.findViewById(R.id.date);
            linearLayout = itemView.findViewById(R.id.LL);
        }
    }

    public class ViewHolderChatUserT extends RecyclerView.ViewHolder {
        private ImageView logo, seeImg;
        private TextView message, date;
        private LinearLayout linearLayout;

        public ViewHolderChatUserT(View itemView) {
            super(itemView);
            logo = itemView.findViewById(R.id.logo);
            message = itemView.findViewById(R.id.message);
            date = itemView.findViewById(R.id.date);
            linearLayout = itemView.findViewById(R.id.LL);
            seeImg = itemView.findViewById(R.id.seenImg);
        }
    }

    public class ViewHolderChatUserP extends RecyclerView.ViewHolder {
        private ImageView logo, img;
        private TextView date;
        private LinearLayout linearLayout;

        public ViewHolderChatUserP(View itemView) {
            super(itemView);
            logo = itemView.findViewById(R.id.logo);
            img = itemView.findViewById(R.id.messageImg);
            img.setLayoutParams(new LinearLayout.LayoutParams(400, 400));
            date = itemView.findViewById(R.id.date);
            linearLayout = itemView.findViewById(R.id.LL);
        }
    }

    public class ViewHolderChatUserM extends RecyclerView.ViewHolder {
        private ImageView logo, seenImg;
        private TextView date, nameMusic, finish;
        private BubbleSeekBar seekBar;
        private ImageButton but;
        private LinearLayout linearLayout;

        public ViewHolderChatUserM(View itemView) {
            super(itemView);
            logo = itemView.findViewById(R.id.logo);
            date = itemView.findViewById(R.id.date);
            but = itemView.findViewById(R.id.butControl);
            seekBar = itemView.findViewById(R.id.seekBar);
            nameMusic = itemView.findViewById(R.id.nameMusic);
            linearLayout = itemView.findViewById(R.id.LL);
            seenImg = itemView.findViewById(R.id.seenImg);
            finish = itemView.findViewById(R.id.finish);
        }
    }

    public class ViewHolderChatFriendM extends RecyclerView.ViewHolder {
        private ImageView logo;
        private TextView date, nameMusic, finish;
        private BubbleSeekBar seekBar;
        private ImageButton but;
        private LinearLayout linearLayout;

        public ViewHolderChatFriendM(View itemView) {
            super(itemView);
            logo = itemView.findViewById(R.id.logo);
            date = itemView.findViewById(R.id.date);
            but = itemView.findViewById(R.id.butControl);
            seekBar = itemView.findViewById(R.id.seekBar);
            nameMusic = itemView.findViewById(R.id.nameMusic);
            linearLayout = itemView.findViewById(R.id.LL);
            finish = itemView.findViewById(R.id.finish);
        }
    }


}
