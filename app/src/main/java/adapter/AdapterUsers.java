package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import helper.StatusUser;
import model.Users;
import progcha.nosenko.com.ActivityProfile;
import progcha.nosenko.com.ActivityProfileUser;
import progcha.nosenko.com.R;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class AdapterUsers extends RecyclerView.Adapter<AdapterUsers.ViewHolderUse> {
    private Context context;
    private DatabaseReference reference;
    private ChildEventListener childEventListener;
    private List<Users> usersList = new ArrayList<>();
    private boolean idSet = false;
    private FirebaseUser currentUser;

    public AdapterUsers(Context context, DatabaseReference ref) {
        this.context = context;
        this.reference = ref;
        this.currentUser = FirebaseAuth.getInstance().getCurrentUser();

        ChildEventListener listener = new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                Log.d("AdapterUsers", "onChildAdded:" + dataSnapshot.getKey());
                Users user = dataSnapshot.getValue(Users.class);
                if (!user.getUid().equals(currentUser.getUid())) {
                    usersList.add(user);
                    notifyItemInserted(usersList.size() - 1);

                    idSet = true;
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Log.d("AdapterUsers", "onChildChanged:" + dataSnapshot.getKey());
                Users user = dataSnapshot.getValue(Users.class);
                String commentKey = dataSnapshot.getKey();
                int commentIndex = usersList.indexOf(commentKey);
                if (commentIndex > -1) {
                    // Replace with the new data
                    usersList.set(commentIndex, user);

                    // Update the RecyclerView
                    notifyItemChanged(commentIndex);

                    idSet = true;
                }
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };

        ref.addChildEventListener(listener);
        this.childEventListener = listener;
    }

    @NonNull
    @Override
    public ViewHolderUse onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_users, parent, false);
        return new ViewHolderUse(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderUse holder, int position) {

        final Users user = usersList.get(position);
        holder.name.setText(user.getName());

        final String UID = reference.getRef().getKey();

        if (!user.getThumb().isEmpty())
            Picasso.get().load(user.getThumb()).placeholder(R.drawable.default_avatar).into(holder.logo);

        if (user.getStatusNet() == StatusUser.StatusNet.ONLINE) {
            holder.status.setText("онлайн");
            holder.indicator.setImageResource(R.drawable.online);
            holder.logo.setBorderColor(context.getResources().getColor(R.color.green));
        } else {
            holder.status.setText("офлайн");
            holder.indicator.setImageResource(R.drawable.offline);
            holder.logo.setBorderColor(context.getResources().getColor(R.color.colorAccent));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("ref", user.getUid());
                Intent intent = new Intent(context, ActivityProfileUser.class);
                intent.putExtra("UID", user.getUid());
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }

    public void cleanupListener() {
        if (childEventListener != null) {
            reference.removeEventListener(childEventListener);
        }
    }

    public boolean isIdSet() {
        return idSet;
    }

    public class ViewHolderUse extends RecyclerView.ViewHolder {
        private TextView name, status;
        private ImageView indicator;
        private CircleImageView logo;

        public ViewHolderUse(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            logo = itemView.findViewById(R.id.logo);
            indicator = itemView.findViewById(R.id.indicatorStatus);
            status = itemView.findViewById(R.id.statusNet);
        }
    }
}
