package adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import helper.StatusUser;
import model.LastMessageChat;
import progcha.nosenko.com.ActivityChat;
import progcha.nosenko.com.ActivityChatGroup;
import progcha.nosenko.com.R;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class AdapterLastMes extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<LastMessageChat> lastMessageChats;
    private Context context;

    private enum TYPE_VIEW {
        dialog_user(0), dialog_group(1);

        private int type;

        TYPE_VIEW(int i) {
            this.type = i;
        }

        public int getType() {
            return type;
        }
    }

    public AdapterLastMes(final List<LastMessageChat> lastMessageChats, Context context) {
        this.lastMessageChats = lastMessageChats;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        RecyclerView.ViewHolder holder = null;

        switch (viewType) {
            case 0:
                view = LayoutInflater.from(context).inflate(R.layout.item_last_chat, parent, false);
                holder = new ViewHolderLastChat(view);
                break;

            case 1:
                view = LayoutInflater.from(context).inflate(R.layout.item_last_message_group_chat, parent, false);
                holder = new ViewHolderLastGroupChat(view);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        final LastMessageChat lastMessageChat = lastMessageChats.get(position);

        switch (getItemViewType(position)) {
            case 0:
                final ViewHolderLastChat viewHolderLastChat = (ViewHolderLastChat) holder;

                if (lastMessageChat.getMessage().length() > 20)
                    viewHolderLastChat.message.setText(lastMessageChat.getMessage().substring(0, 16) + " ...");
                else
                    viewHolderLastChat.message.setText(lastMessageChat.getMessage());

                viewHolderLastChat.date.setText(lastMessageChat.getTime());
                viewHolderLastChat.name.setText(lastMessageChat.getName());

                if (lastMessageChat.getLogo() != null)
                    try {
                        Picasso.get().load(lastMessageChat.getLogo()).placeholder(R.drawable.default_avatar).into(viewHolderLastChat.logo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                if (lastMessageChat.getStatusNet().equals(StatusUser.StatusNet.OFLINE))
                    viewHolderLastChat.logo.setBorderColor(context.getResources().getColor(R.color.colorAccent));
                else
                    viewHolderLastChat.logo.setBorderColor(context.getResources().getColor(R.color.green));

                viewHolderLastChat.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent2 = new Intent(context, ActivityChat.class);
                        intent2.putExtra("name", lastMessageChat.getName());
                        intent2.putExtra("UID", lastMessageChat.getFriendUID());
                        intent2.putExtra("thumb", lastMessageChat.getLogo());
                        intent2.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent2);
                    }
                });
                break;

            case 1:
                final ViewHolderLastGroupChat viewHolderLastGroupChat = (ViewHolderLastGroupChat) holder;

                viewHolderLastGroupChat.counter.setText(lastMessageChat.getCountUsers() + "");
                viewHolderLastGroupChat.name.setText(lastMessageChat.getName());
                viewHolderLastGroupChat.date.setText(lastMessageChat.getTime());
                viewHolderLastGroupChat.nameUserLast.setText(lastMessageChat.getNameUserLast());

                if (lastMessageChat.getLastMessage() != null)
                    if (lastMessageChat.getLastMessage().length() > 20)
                        viewHolderLastGroupChat.message.setText(lastMessageChat.getLastMessage().substring(0, 16) + " ...");
                    else
                        viewHolderLastGroupChat.message.setText(lastMessageChat.getLastMessage());

                if (lastMessageChat.getNameUserLast() != null)
                    viewHolderLastGroupChat.nameUserLast.setText(lastMessageChat.getNameUserLast() + ":");

                if (lastMessageChat.getLogo() != null)
                    try {
                        Picasso.get().load(lastMessageChat.getLogo()).placeholder(R.drawable.default_avatar).into(viewHolderLastGroupChat.logo);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                viewHolderLastGroupChat.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent2 = new Intent(context, ActivityChatGroup.class);
                        intent2.putExtra("nameChat", lastMessageChat.getName());
                        intent2.putExtra("groupChatUID", lastMessageChat.getGroupChatUID());
                        intent2.putExtra("thumb", lastMessageChat.getThumb());
                        intent2.putExtra("countUsers", lastMessageChat.getCountUsers());
                        intent2.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent2);
                    }
                });

                break;
        }

    }

    @Override
    public int getItemCount() {
        return lastMessageChats.size();
    }

    public List<LastMessageChat> getLastMessageChats() {
        return lastMessageChats;
    }

    public void setLastMessageChats(List<LastMessageChat> lastMessageChats) {
        this.lastMessageChats = lastMessageChats;
    }

    public void clearGroupChat() {
        if (lastMessageChats != null)
            for (int i = 0; i < lastMessageChats.size(); i++) {
                if (lastMessageChats.get(i).getGroupChatUID() != null)
                    lastMessageChats.remove(i);
            }
    }

    public void clearChat() {
        if (lastMessageChats != null)
            for (int i = 0; i < lastMessageChats.size(); i++) {
                if (lastMessageChats.get(i).getGroupChatUID() == null)
                    lastMessageChats.remove(i);
            }
    }

    @Override
    public int getItemViewType(int position) {
        if (lastMessageChats.get(position).getGroupChatUID() != null) {
            return TYPE_VIEW.dialog_group.getType();
        } else return TYPE_VIEW.dialog_user.getType();
    }

    public class ViewHolderLastChat extends RecyclerView.ViewHolder {
        private CircleImageView logo;
        private EditText message;
        private TextView date, name;

        public ViewHolderLastChat(View itemView) {
            super(itemView);
            logo = itemView.findViewById(R.id.logo);
            message = itemView.findViewById(R.id.message);
            date = itemView.findViewById(R.id.date);
            name = itemView.findViewById(R.id.name);
        }
    }

    private class ViewHolderBorder extends RecyclerView.ViewHolder {
        public ViewHolderBorder(View itemView) {
            super(itemView);
        }
    }

    public class ViewHolderLastGroupChat extends RecyclerView.ViewHolder {
        private CircleImageView logo;
        private EditText message;
        private TextView date, name, counter, nameUserLast;

        public ViewHolderLastGroupChat(View itemView) {
            super(itemView);
            logo = itemView.findViewById(R.id.logo);
            message = itemView.findViewById(R.id.message);
            date = itemView.findViewById(R.id.date);
            name = itemView.findViewById(R.id.nameGroupChat);
            counter = itemView.findViewById(R.id.counterUser);
            nameUserLast = itemView.findViewById(R.id.nameUserLast);
        }
    }
}
