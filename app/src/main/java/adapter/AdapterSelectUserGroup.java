package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import model.SelectUser;
import model.User;
import progcha.nosenko.com.R;

public class AdapterSelectUserGroup extends RecyclerView.Adapter<AdapterSelectUserGroup.ViewHolderSelectUser> {
    private List<SelectUser> users;
    private Context context;

    public AdapterSelectUserGroup(List<SelectUser> users, Context context) {
        this.users = users;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolderSelectUser onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_select_user_to_group, parent, false);
        return new  ViewHolderSelectUser(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderSelectUser holder, int position) {

        final SelectUser user = users.get(position);

        holder.name.setText(user.getName());
        holder.check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    user.setCheck(true);
                else
                    user.setCheck(false);
            }
        });
    }

    public List<SelectUser> getSelected()
    {
        List<SelectUser> selectUsers = new ArrayList<>();
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).isCheck())
                selectUsers.add(users.get(i));
        }

        return selectUsers;
    }


    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolderSelectUser extends RecyclerView.ViewHolder {
        private TextView name;
        private CheckBox check;

        public ViewHolderSelectUser(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.nameUser);
            check = itemView.findViewById(R.id.check);
        }
    }
}
