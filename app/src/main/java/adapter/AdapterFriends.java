package adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import helper.StatusUser;
import model.User;
import model.Users;
import progcha.nosenko.com.ActivityChat;
import progcha.nosenko.com.ActivityProfileUser;
import progcha.nosenko.com.R;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static helper.Helper.setStatusNet;
import static helper.StatusUser.StatusNet.OFLINE;

public class AdapterFriends extends RecyclerView.Adapter<AdapterFriends.ViewHolderFriends> {
    private List<User> friends;
    private Context context;

    public AdapterFriends(List<User> friends, Context context) {
        this.friends = friends;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolderFriends onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_reiend, parent, false);
        return new ViewHolderFriends(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderFriends holder, int position) {
        final User user = friends.get(position);
        holder.name.setText(friends.get(position).getName());

        if (!friends.get(position).getThumb().isEmpty())
            Picasso.get().load(friends.get(position).getThumb()).placeholder(R.drawable.default_avatar).into(holder.logo);

        if (friends.get(position).getStatusNet() == StatusUser.StatusNet.ONLINE) {
            holder.status.setText("онлайн");
            holder.indicator.setImageResource(R.drawable.online);
            holder.logo.setBorderColor(context.getResources().getColor(R.color.green));
        } else {
            holder.status.setText("офлайн");
            holder.indicator.setImageResource(R.drawable.offline);
            holder.logo.setBorderColor(context.getResources().getColor(R.color.colorAccent));
        }

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context).setTitle("меню").setItems(new CharSequence[]{"Профиль", "Сообщения"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                Intent intent = new Intent(context, ActivityProfileUser.class);
                                intent.putExtra("UID", user.getUID());
                                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);
                            case 1:
                                Intent intent2 = new Intent(context, ActivityChat.class);
                                intent2.putExtra("name", user.getName());
                                intent2.putExtra("UID", user.getUID());
                                intent2.putExtra("thumb", user.getThumb());
                                intent2.setFlags(FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent2);

                        }
                    }
                });

                builder.show();
                return true;
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context).setTitle("меню").setItems(new CharSequence[]{"Профиль", "Сообщения"}, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i) {
                            case 0:
                                Intent intent = new Intent(context, ActivityProfileUser.class);
                                intent.putExtra("UID", user.getUID());
                                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent);
                            case 1:
                                Intent intent2 = new Intent(context, ActivityChat.class);
                                intent2.putExtra("name", user.getName());
                                intent2.putExtra("UID", user.getUID());
                                intent2.putExtra("thumb", user.getThumb());
                                intent2.setFlags(FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(intent2);

                        }
                    }
                });

                builder.show();
//                Intent intent2 = new Intent(context, ActivityChat.class);
//                intent2.putExtra("name", user.getNameChat());
//                intent2.putExtra("UID", user.getUID());
//                intent2.putExtra("thumb", user.getThumb());
//                intent2.setFlags(FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(intent2);
            }
        });
    }

    @Override
    public int getItemCount() {
        return friends.size();
    }

    public class ViewHolderFriends extends RecyclerView.ViewHolder {
        private TextView name, status;
        private ImageView indicator;
        private CircleImageView logo;

        public ViewHolderFriends(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            logo = itemView.findViewById(R.id.logo);
            indicator = itemView.findViewById(R.id.indicatorStatus);
            status = itemView.findViewById(R.id.statusNet);
        }
    }
}
