package adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import helper.StatusUser;
import model.Request;
import model.StateUser;
import progcha.nosenko.com.R;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdapterRequest extends RecyclerView.Adapter<AdapterRequest.ViewHolderRequest> {
    private Context context;
    private List<Request> requests;
    private FirebaseUser currentUser;
    private DatabaseReference friendRequest;

    public AdapterRequest(Context context, List<Request> requests) {
        this.context = context;
        this.requests = requests;
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        friendRequest = FirebaseDatabase.getInstance().getReference().child("request");
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public ViewHolderRequest onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_request, parent, false);
        return new ViewHolderRequest(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderRequest holder, int position) {
        final Request request = requests.get(position);

        holder.name.setText(request.getName());

        switch (request.getRequestType()) {
            case send:
                holder.linearLayout.removeView(holder.but);

                holder.but.setText("отменить запрос");
                holder.linearLayout.addView(holder.but);
                holder.subscrute.setText("предлагаете дружбу");
                holder.but.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
                break;

            case received:
                holder.linearLayout.removeView(holder.but);

                holder.but.setText("подтвердить дружбу");
                holder.linearLayout.addView(holder.but);
                holder.subscrute.setText("предлагает дружбу");
                holder.but.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Map data = new HashMap();
                        data.put("/" + currentUser.getUid() + "/" + request.getUID() + "/" + "requestType", StateUser.REQUEST_TYPE.friend);
                        data.put("/" + request.getUID() + "/" + currentUser.getUid() + "/" + "requestType", StateUser.REQUEST_TYPE.friend);

                        friendRequest.updateChildren(data, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                friendMyList.put(request.getUID(), StateUser.REQUEST_TYPE.friend);
                                frieds.updateChildren(friendMyList, new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                        userRef.child("friends").child(currentUser.getUid()).setValue(StateUser.REQUEST_TYPE.friend).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    butDel.setEnabled(true);

                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
                break;

            case subscriber:
                holder.linearLayout.removeView(holder.but);

                holder.subscrute.setText("подписаны");
                holder.linearLayout.addView(holder.but);
                holder.but.setText("отписаться");
                break;

            case none:
                holder.linearLayout.removeView(holder.but);

                holder.subscrute.setText("подписчик");

                break;
        }

        if (request.getStatusNet() == StatusUser.StatusNet.ONLINE) {
            // holder.status.setText("онлайн");
            // holder.indicator.setImageResource(R.drawable.online);
            holder.logo.setBorderColor(context.getResources().getColor(R.color.green));
        } else {
            // holder.status.setText("офлайн");
            //holder.indicator.setImageResource(R.drawable.offline);
            holder.logo.setBorderColor(context.getResources().getColor(R.color.colorAccent));
        }

        if (!requests.get(position).getLogo().isEmpty())
            Picasso.get().load(requests.get(position).getLogo()).placeholder(R.drawable.default_avatar).into(holder.logo);
    }

    @Override
    public int getItemCount() {
        return requests.size();
    }

    public class ViewHolderRequest extends RecyclerView.ViewHolder {
        private TextView name, status;
        //  private ImageView indicator;
        private CircleImageView logo;
        //   private Button button;
        private TextView subscrute;
        private LinearLayout linearLayout;
        private Button but;

        public ViewHolderRequest(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            logo = itemView.findViewById(R.id.logo);
            //  indicator = itemView.findViewById(R.id.indicatorStatus);
            // status = itemView.findViewById(R.id.statusNet);
            but = (Button) LayoutInflater.from(context).inflate(R.layout.but_request_item, null, false);
            but.setPadding(20, 0, 20, 0);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, 38);
            params.setMargins(0, 0, 0, 3);
            params.gravity = Gravity.RIGHT;
            but.setLayoutParams(params);
            subscrute = itemView.findViewById(R.id.subscrute);
            linearLayout = itemView.findViewById(R.id.LL);
        }
    }
}
