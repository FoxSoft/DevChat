package progcha.nosenko.com;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flaviofaria.kenburnsview.KenBurnsView;
import com.github.florent37.diagonallayout.DiagonalLayout;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.*;
import com.sdsmdg.tastytoast.TastyToast;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import helper.Helper;
import helper.StatusUser;
import model.Notifications;
import model.StateUser;
import model.User;

public class ActivityProfileUser extends AppCompatActivity {
    private String UID;
    private TextView name, status, statusNet, countFrieds;
    private ImageView thumb, indicator;
    private KenBurnsView backHeader;
    private FirebaseUser currentUser;
    private DatabaseReference userRef, frieds, notifi, friendRequest;
    private User user;
    private Button butAdd, butDel;
    private Map friendMyList;
    private boolean isFriend = false;
    private FrameLayout frameAdd, frameDell;
    private DiagonalLayout diagonalLayout;

    private StateUser.REQUEST_TYPE requestType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_user);

        name = findViewById(R.id.name);
        status = findViewById(R.id.status);
        thumb = findViewById(R.id.thumb);
        backHeader = findViewById(R.id.backHeader);
        statusNet = findViewById(R.id.statusNet);
        indicator = findViewById(R.id.indicatorStatus);
        butAdd = findViewById(R.id.butAdd);
        butDel = findViewById(R.id.butDel);
        frameAdd = findViewById(R.id.frameAdd);
        frameDell = findViewById(R.id.frameDell);
        countFrieds = findViewById(R.id.countFreinds);
        diagonalLayout = findViewById(R.id.diagonalLayout);

        diagonalLayout.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                Helper.getSizeDisplay(getWindowManager(), Helper.DISPLAY.height) - 300));

        requestType = StateUser.REQUEST_TYPE.send;

        if (getIntent().getExtras() != null) {
            UID = getIntent().getExtras().getString("UID");

            UID = getIntent().getStringExtra("UID");
            currentUser = FirebaseAuth.getInstance().getCurrentUser();
            userRef = FirebaseDatabase.getInstance().getReference().child("users").child(UID);
            frieds = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid()).child("friends");
            notifi = FirebaseDatabase.getInstance().getReference().child("notifications");
            friendRequest = FirebaseDatabase.getInstance().getReference().child("request");

            user = new User();
            user.setUID(UID);

            friendRequest.child(currentUser.getUid()).child(user.getUID()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Log.d("requestUser", "exists " + dataSnapshot.exists());
                    if (dataSnapshot.exists()) {

                        Log.d("requestUser", dataSnapshot.child("requestType").getValue(StateUser.REQUEST_TYPE.class) + "");
                        requestType = dataSnapshot.child("requestType").getValue(StateUser.REQUEST_TYPE.class);

                        switch (requestType) {
                            case send:
                                butDel.setText("удалить запрос");
                                butAdd.setEnabled(false);
                                frameAdd.setForeground(getResources().getDrawable(R.drawable.back_white_proz));
                                butDel.setEnabled(true);
                                frameDell.setForeground(getResources().getDrawable(R.drawable.back_proz));
                                break;
                            case friend:
                                butDel.setText("удалить из друзей");
                                butAdd.setEnabled(false);
                                frameAdd.setForeground(getResources().getDrawable(R.drawable.back_white_proz));
                                butDel.setEnabled(true);
                                frameDell.setForeground(getResources().getDrawable(R.drawable.back_proz));
                                break;
                            case received:
                                butDel.setText("оставить в подписчиках");
                                butAdd.setText("подтведить дружбу");
                                butAdd.setEnabled(true);
                                frameAdd.setForeground(getResources().getDrawable(R.drawable.back_proz));
                                butDel.setEnabled(true);
                                frameDell.setForeground(getResources().getDrawable(R.drawable.back_proz));
                                break;
                            case subscriber:
                                butDel.setText("отписаться");
                                butAdd.setEnabled(false);
                                frameAdd.setForeground(getResources().getDrawable(R.drawable.back_white_proz));
                                butDel.setEnabled(true);
                                frameDell.setForeground(getResources().getDrawable(R.drawable.back_proz));
                                break;
                            default:
                                butAdd.setText("добавить");
                                butDel.setText("удалить");
                                butAdd.setEnabled(true);
                                butDel.setEnabled(false);
                                frameAdd.setForeground(getResources().getDrawable(R.drawable.back_proz));
                                frameDell.setForeground(getResources().getDrawable(R.drawable.back_white_proz));

                        }

                    } else {
                        butAdd.setText("добавить");
                        butDel.setText("удалить");
                        butAdd.setEnabled(true);
                        butDel.setEnabled(false);
                        frameAdd.setForeground(getResources().getDrawable(R.drawable.back_proz));
                        frameDell.setForeground(getResources().getDrawable(R.drawable.back_white_proz));
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            userRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    user.setName(dataSnapshot.child("name").getValue().toString());
                    name.setText(user.getName());
                    status.setText(dataSnapshot.child("status").getValue().toString());
                    user.setCountFriends((int) dataSnapshot.child("friends").getChildrenCount());
                    countFrieds.setText(user.getCountFriends() == 0 ? "---" : user.getCountFriends() + "");

                    if (dataSnapshot.child("statusNet").getValue().toString().equals("ONLINE")) {
                        indicator.setImageResource(R.drawable.online);
                        statusNet.setText("онлайн");
                    } else {
                        indicator.setImageResource(R.drawable.offline);
                        statusNet.setText("офлайн");
                    }

                    if (!dataSnapshot.child("idLogo").getValue().toString().equals("")) {
                        Picasso.get().load(dataSnapshot.child("idLogo").getValue().toString()).placeholder(R.drawable.default_avatar).into(thumb);
                        Picasso.get().load(dataSnapshot.child("idLogo").getValue().toString()).placeholder(R.drawable.default_avatar).into(backHeader);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            butAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    butAdd.setEnabled(false);
                    if (requestType.equals(StateUser.REQUEST_TYPE.send)) {
                        friendRequest.child(currentUser.getUid()).child(user.getUID()).child("requestType").setValue(StateUser.REQUEST_TYPE.send).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    friendRequest.child(user.getUID()).child(currentUser.getUid()).child("requestType").setValue(StateUser.REQUEST_TYPE.received)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
                                                    Notifications notifications = new Notifications(currentUser.getUid(), user.getUID(), Notifications.TYPE_NOTIFI.REQUEST,
                                                            "123", currentUser.getDisplayName() + " предлагает дружбу");
                                                    notifi.child(user.getUID()).push().setValue(notifications).addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {
                                                            TastyToast.makeText(getApplicationContext(), "Теперь " + user.getName() + " ваш друг", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show();
                                                            butDel.setEnabled(true);
                                                            frameDell.setForeground(getResources().getDrawable(R.drawable.back_proz));
                                                            frameAdd.setForeground(getResources().getDrawable(R.drawable.back_white_proz));
                                                        }
                                                    });
                                                }
                                            });
                                } else {
                                    TastyToast.makeText(getApplicationContext(), "повторите снова", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show();
                                }
                            }
                        });

                    }

                    if (requestType.equals(StateUser.REQUEST_TYPE.received)) {
                        Map data = new HashMap();
                        data.put("/" + currentUser.getUid() + "/" + user.getUID() + "/" + "requestType", StateUser.REQUEST_TYPE.friend);
                        data.put("/" + user.getUID() + "/" + currentUser.getUid() + "/" + "requestType", StateUser.REQUEST_TYPE.friend);

                        friendRequest.updateChildren(data, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                friendMyList.put(user.getUID(), StateUser.REQUEST_TYPE.friend);
                                frieds.updateChildren(friendMyList, new DatabaseReference.CompletionListener() {
                                    @Override
                                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                        userRef.child("friends").child(currentUser.getUid()).setValue(StateUser.REQUEST_TYPE.friend).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    butDel.setEnabled(true);
                                                    frameDell.setForeground(getResources().getDrawable(R.drawable.back_proz));
                                                    frameAdd.setForeground(getResources().getDrawable(R.drawable.back_white_proz));
                                                    butDel.setText("удалить из друзей");
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                }
            });

            butDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    butDel.setEnabled(false);
                    Map data = new HashMap();
                    switch (requestType) {
                        case subscriber:
                            friendRequest.child(currentUser.getUid()).child(user.getUID()).removeValue(new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                }
                            });

                            friendRequest.child(user.getUID()).child(currentUser.getUid()).removeValue(new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                }
                            });
                            break;

                        case received:
                            data.put("/" + currentUser.getUid() + "/" + user.getUID() + "/" + "requestType", StateUser.REQUEST_TYPE.none);
                            data.put("/" + user.getUID() + "/" + currentUser.getUid() + "/" + "requestType", StateUser.REQUEST_TYPE.subscriber);

                            friendRequest.updateChildren(data, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                }
                            });
                            break;

                        case friend:
                            data.put("/" + currentUser.getUid() + "/" + user.getUID() + "/" + "requestType", StateUser.REQUEST_TYPE.none);
                            data.put("/" + user.getUID() + "/" + currentUser.getUid() + "/" + "requestType", StateUser.REQUEST_TYPE.subscriber);

                            friendRequest.updateChildren(data, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                    frieds.child(user.getUID()).removeValue(new DatabaseReference.CompletionListener() {
                                        @Override
                                        public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                            userRef.child("friends").child(currentUser.getUid()).removeValue();
                                        }
                                    });

                                }
                            });
                            break;

                        case send:
                            friendRequest.child(currentUser.getUid()).child(user.getUID()).removeValue(new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                }
                            });

                            friendRequest.child(user.getUID()).child(currentUser.getUid()).removeValue(new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                }
                            });
                            break;
                    }


                    butAdd.setEnabled(true);
                    frameDell.setForeground(getResources().getDrawable(R.drawable.back_white_proz));
                    frameAdd.setForeground(getResources().getDrawable(R.drawable.back_proz));
                }
            });
        }

        getMyFriends();
    }


    private void getMyFriends() {
        friendMyList = new HashMap<>();
        frieds.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    friendMyList.put(ds.getKey(), StateUser.REQUEST_TYPE.friend);
                    Log.d("counterFriends", ds.getKey() +" s "+friendMyList.size());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Helper.setStatusNet(StatusUser.StatusNet.ONLINE);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            backHeader.setForeground(getDrawable(R.drawable.style_proz_black));
//        }
    }
}
