package progcha.nosenko.com;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.OpenableColumns;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.Util;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapter.AdapterChat;
import de.hdodenhof.circleimageview.CircleImageView;
import helper.Helper;
import helper.StatusUser;
import helper.Time;
import model.Chat;
import model.Notifications;
import model.User;
import servises.ServiseFireBaseMessages;

import static helper.Helper.setStatusNet;
import static helper.StatusUser.StatusNet.OFLINE;
import static helper.StatusUser.StatusNet.ONLINE;

public class ActivityChat extends AppCompatActivity {
    private static final int COUNT_LOAD_ITEMS = 30;
    private static final int GALERY_PICKTURE = 1;
    private static final int MUSIC = 2;
    private static final String LAYOUT_MANAGER_KEY = "LMK";
    private Toolbar toolbar;
    private User friend;
    private ImageView backBut;
    private CircleImageView logo;
    private TextView name, statusNet;
    private ImageButton butAdd, butSend;
    private EditText input;
    private DatabaseReference lastSeen, statusNetRef, refChat, refRut, refMessage;
    private FirebaseUser currentUser;
    private RecyclerView recyclerView;
    private List<Chat> chatList;
    private AdapterChat adapterChat;
    private LinearLayoutManager manager;
    private int currentPage = 1;
    private SwipeRefreshLayout refreshLayout;
    private int itemPos = 0;
    private String lastKey = "";
    private String prevKey = "";
    private StorageReference storageReference;

    private View state;
    private TextView titleState;
    private LinearLayout linearLayout;
    private boolean isAddState = false;
    private Thread ThreadStatusNet;
    private DatabaseReference refChatFriend, updateMessage;
    private BoomMenuButton bmb;
    private int[] idIcon = new int[]{R.drawable.picture, R.drawable.music, R.drawable.video, R.drawable.arhive, R.drawable.file};
    private int[] idColor = new int[]{};
    private String[] titleBut = new String[]{"картинки", "музыка", "видео", "архивы", "файлы"};
    private HashMap seens;
    private Map seensFriend;
    private DatabaseReference updateMessageFriend;
    private Map leesen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");

        View header = LayoutInflater.from(getApplicationContext()).inflate(R.layout.custom_toolbar, null, false);
        logo = header.findViewById(R.id.logo);
        name = header.findViewById(R.id.name);
        statusNet = header.findViewById(R.id.statusNet);
        backBut = header.findViewById(R.id.backButImg);
        refreshLayout = findViewById(R.id.swipeRef);

        initBmb();

        Bundle bundle = getIntent().getExtras();

        if (getIntent().getExtras() != null) {
            friend = new User();
            friend.setName(getIntent().getExtras().getString("name"));
            friend.setThumb(getIntent().getExtras().getString("thumb"));
            friend.setUID(getIntent().getExtras().getString("UID"));

            lastSeen = FirebaseDatabase.getInstance().getReference().child("users").child(friend.getUID()).child("lastSeen");
            refRut = FirebaseDatabase.getInstance().getReference().getRoot();
            storageReference = FirebaseStorage.getInstance().getReference();
            statusNetRef = lastSeen.getParent();
            statusNetRef.child("statusNet");
            currentUser = FirebaseAuth.getInstance().getCurrentUser();

            statusNetRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
// intent2.putExtra("statusNet", user.getStatusNet() == StatusUser.StatusNet.ONLINE ? "онлайн" : "офлайн");
                    if (dataSnapshot.child("statusNet").getValue(StatusUser.StatusNet.class) == StatusUser.StatusNet.ONLINE) {
                        statusNet.setText("онлайн");
                        logo.setBorderColor(getResources().getColor(R.color.green));
                    } else {
                        lastSeen.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                friend.setLastSeen(dataSnapshot.getValue().toString());
                                long longTime = Long.parseLong(friend.getLastSeen());
                                statusNet.setText(Time.getTimeAgo(longTime, getApplicationContext()));
                                logo.setBorderColor(getResources().getColor(R.color.colorAccent));
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


            name.setText(friend.getName());

            if (!friend.getThumb().isEmpty())
                Picasso.get().load(friend.getThumb()).placeholder(R.drawable.default_avatar).into(logo);

            backBut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
            toolbar.addView(header);
            setSupportActionBar(toolbar);

            // butAdd = findViewById(R.id.butAdd);
            butSend = findViewById(R.id.butSend);
            input = findViewById(R.id.input);
            recyclerView = findViewById(R.id.recycler);
            linearLayout = findViewById(R.id.LL);
            recyclerView.setHasFixedSize(true);
            manager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(manager);

            createCheangeInput();

            chatList = new ArrayList<>();
            seens = new HashMap<>();
            seensFriend = new HashMap();
            leesen = new HashMap();

            refChat = lastSeen.getParent().getParent().getParent();
            refChat.child("chat").child(currentUser.getUid()).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.hasChild(friend.getUID())) {

                        Map hashMapUser = new HashMap();
                        hashMapUser.put("timestamp", Helper.getTimeNow());
                        hashMapUser.put("message", "");
                        hashMapUser.put("seen", true);
                        hashMapUser.put("state", Chat.STATE.none);

                        Map hashMapFriend = new HashMap();
                        hashMapFriend.put("timestamp", Helper.getTimeNow());
                        hashMapFriend.put("message", "");
                        hashMapFriend.put("seen", false);
                        hashMapFriend.put("state", Chat.STATE.none);

                        Map chatUserMap = new HashMap();
                        chatUserMap.put("chat/" + currentUser.getUid() + "/" + friend.getUID(), hashMapUser);
                        chatUserMap.put("chat/" + friend.getUID() + "/" + currentUser.getUid(), hashMapFriend);

                        Log.d("Chat_Log", "update");
                        refChat.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                if (databaseError != null) {
                                    Log.d("Chat_Log", databaseError.getMessage());
                                }
                            }
                        });
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            updateMessage = refRut.child("messages").child(currentUser.getUid()).child(friend.getUID());

            updateMessageFriend = refRut.child("messages").child(friend.getUID()).child(currentUser.getUid());

            refChatFriend = FirebaseDatabase.getInstance().getReference().child("chat").child(friend.getUID()).child(currentUser.getUid());
            refChatFriend.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    adapterChat.updateSeeItem(dataSnapshot.child("seen").getValue(Boolean.class));
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            butSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sendMessage();
                }
            });

//            butAdd.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent intent = new Intent();
//                    intent.setType("image/*");
//                    intent.setAction(Intent.ACTION_GET_CONTENT);
//                    startActivityForResult(Intent.createChooser(intent, "Выбрать фото"), GALERY_PICKTURE);
//                }
//            });

            adapterChat = new AdapterChat(chatList, getApplicationContext(), friend.getUID());
            adapterChat.setHasStableIds(true);
            recyclerView.setAdapter(adapterChat);

            loadMessage();

            refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    currentPage++;
                    itemPos = 0;
                    loadMoreMessage();
                }
            });

            createStateUser();
            getStateFriend();
        } else {
            Intent intent = new Intent(getApplicationContext(), ServiseFireBaseMessages.class);
            startActivity(intent);
        }
    }

    private void updateMessageFriend() {
        if (seensFriend != null)
            updateMessageFriend.updateChildren(seensFriend, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                }
            });

    }

    private void getMessageNotSeenFriend() {
        updateMessageFriend.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                if (dataSnapshot.child("from").getValue().toString().equals(friend.getUID()) & !dataSnapshot.child("seen").getValue(Boolean.class)) {
                    seensFriend.put(dataSnapshot.getKey() + "/seen", true);
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Log.d("leesen onChildChanged", "myMess" + dataSnapshot.getKey());
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void initBmb() {

        bmb = findViewById(R.id.bmb);
        bmb.setBackgroundEffect(true);

        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            TextInsideCircleButton.Builder builder = new TextInsideCircleButton.Builder()
                    .normalImageRes(idIcon[i])
                    // .highlightedText(titleBut[i])
                    .shadowEffect(true)
                    .shadowOffsetX(15)
                    .buttonCornerRadius(20)
                    .normalColor(getResources().getColor(R.color.colorPrimaryDark))
                    .highlightedColor(getResources().getColor(R.color.colorAccent))
                    .shadowOffsetY(5)
                    .shadowRadius(Util.dp2px(10))
                    .shadowCornerRadius(Util.dp2px(20))
                    .shadowColor(Color.parseColor("#AA000000"))
                    .imagePadding(new Rect(5, 5, 5, 5))
                    .isRound(false);

            bmb.addBuilder(builder);
            bmb.setOnBoomListener(new OnBoomListener() {
                @Override
                public void onClicked(int index, BoomButton boomButton) {
                    Log.d("clickBoom", "onClicked" + index);
                    Intent intent = new Intent();
                    switch (index) {
                        case 0:

                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Выбрать фото"), GALERY_PICKTURE);
                            break;

                        case 1:
                            intent.setType("audio/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Выбрать музыку"), MUSIC);
                            break;
                    }
                }

                @Override
                public void onBackgroundClick() {

                }

                @Override
                public void onBoomWillHide() {

                }

                @Override
                public void onBoomDidHide() {

                }

                @Override
                public void onBoomWillShow() {

                }

                @Override
                public void onBoomDidShow() {

                }
            });
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private void updateChatSeen(boolean seen) {
        Map chat = new HashMap();
        chat.put("chat/" + currentUser.getUid() + "/" + friend.getUID() + "/seen", seen);

        refRut.updateChildren(chat, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapterChat.destoryPlayer();
        updateChatSeen(false);
        Helper.setStatusNet(OFLINE);
        // updateMessageSeen();

        updateMessageFriend();
    }

    private void createStateUser() {
        state = LayoutInflater.from(getApplicationContext()).inflate(R.layout.layout_input_user, null);
        titleState = state.findViewById(R.id.title);

    }

    private void getStateFriend() {
        refChat.child("chat").child(friend.getUID()).child(currentUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Chat.STATE state = dataSnapshot.child("state").getValue(Chat.STATE.class);
                if (state == Chat.STATE.none) {
                    try {
                        delStateUser();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else try {
                    addStateUser();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void addStateUser() {
        isAddState = true;
        titleState.setText(friend.getName() + " набирает...");
        linearLayout.addView(state, 0, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void delStateUser() {
        isAddState = false;
        linearLayout.removeView(state);
    }

    private void createCheangeInput() {
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("inputState", "beforeTextChanged");

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int q = 0;
                if (!charSequence.equals("")) {
                    if (i != q)
                        updateStateUser(Chat.STATE.write);
                    q = i;
                }
                if (i == 0) {
                    updateStateUser(Chat.STATE.none);
                }

                Log.d("inputState1", "" + i);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                Log.d("inputState", "afterTextChanged");
            }
        });
    }

    private void loadMoreMessage() {
        DatabaseReference refMessage = refRut.child("messages").child(currentUser.getUid()).child(friend.getUID());
        Query messageQuery = refMessage.orderByKey().endAt(lastKey).limitToLast(10);
        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Chat chat = dataSnapshot.getValue(Chat.class);
                String messageKey = dataSnapshot.getKey();

                if (!prevKey.contains(messageKey)) {
                    chatList.add(itemPos++, chat);
                } else {
                    prevKey = lastKey;
                }

                if (itemPos == 1) {
                    lastKey = messageKey;
                }
                adapterChat.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
                manager.scrollToPositionWithOffset(10, 0);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void loadMessage() {
        refMessage = refRut.child("messages").child(currentUser.getUid()).child(friend.getUID());
        Query messageQuery = refMessage.limitToLast(currentPage * COUNT_LOAD_ITEMS);
        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Chat chat = dataSnapshot.getValue(Chat.class);
                itemPos++;
                if (itemPos == 1) {
                    String messageKey = dataSnapshot.getKey();
                    lastKey = messageKey;
                    prevKey = messageKey;
                }

                Log.d("type_message", chat.getType().toString());
                chatList.add(chat);

                adapterChat.notifyDataSetChanged();
                recyclerView.scrollToPosition(chatList.size() - 1);
                refreshLayout.setRefreshing(false);

                Log.d("chatList", chatList.toString());
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void sendMessage() {

        if (!TextUtils.isEmpty(input.getText().toString())) {
            DatabaseReference messageRef = refRut.child("messages").child(currentUser.getUid()).child(friend.getUID()).push();
            String pushId = messageRef.getKey();

            final Chat chat = new Chat();
            chat.setMessage(input.getText().toString());
            chat.setSeend(false);
            chat.setTimeNow();
            chat.setType(Chat.TYPE_MESSAGE.text);
            chat.setFrom(currentUser.getUid());
            chat.setSeen(false);

            Map map = new HashMap();
            map.put("messages" + "/" + currentUser.getUid() + "/" + friend.getUID() + "/" + pushId, chat);
            map.put("messages" + "/" + friend.getUID() + "/" + currentUser.getUid() + "/" + pushId, chat);

            input.setText("");
            refRut.updateChildren(map, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                }
            });

            //DatabaseReference chatMessage = refRut.child("chat").child(currentUser.getUid()).child(friend.getUID()).push();

            Map chats = new HashMap();
            chats.put("chat/" + currentUser.getUid() + "/" + friend.getUID() + "/message", "Я: " + chat.getMessage());
            chats.put("chat/" + friend.getUID() + "/" + currentUser.getUid() + "/message", chat.getMessage());
            chats.put("chat/" + currentUser.getUid() + "/" + friend.getUID() + "/timestamp", Helper.getTimeNow());
            chats.put("chat/" + friend.getUID() + "/" + currentUser.getUid() + "/timestamp", Helper.getTimeNow());
            chats.put("chat/" + currentUser.getUid() + "/" + friend.getUID() + "/state", Chat.STATE.none);
            chats.put("chat/" + currentUser.getUid() + "/" + friend.getUID() + "/seen", true);


            refRut.updateChildren(chats, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    Helper.pushMessageNotification(new Notifications(currentUser.getUid(), friend.getUID(), Notifications.TYPE_NOTIFI.MESSAGE, chat.getMessage(), friend.getName()));
                }
            });
        }
    }

    private void updateStateUser(Chat.STATE state) {
        Map chat = new HashMap();
        chat.put("chat/" + currentUser.getUid() + "/" + friend.getUID() + "/state", state);
        //  chat.put("chat/" + friend.getUID() + "/" + currentUser.getUid() + "/state", state);

        refRut.updateChildren(chat, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        ThreadStatusNet = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        return;
                    }

                    Log.d("ThreadStatusNet", "Привет из потока " + Thread.currentThread().getName());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setStatusNet(ONLINE);
                            Helper.setLastSeen();
                            ThreadStatusNet.interrupt();
                            // ThreadStatusNet.destroy();
                            Log.d("ThreadStatusNet", "Привет из потока runOnUiThread");
                        }
                    });


                    Log.d("ThreadStatusNet", "сдохни " + Thread.currentThread().getName());
                }
            }
        });

        ThreadStatusNet.start();

        getMessageNotSeenFriend();
    }


    @Override
    protected void onStop() {
        super.onStop();
        setStatusNet(ONLINE);
        updateChatSeen(false);



    }



    @Override
    protected void onResume() {
        super.onResume();
        updateChatSeen(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALERY_PICKTURE && resultCode == RESULT_OK) {
            Uri urlImage = data.getData();
            DatabaseReference userMessagePush = refRut.child("messages").child(currentUser.getUid()).child(friend.getUID()).push();
            final String pushId = userMessagePush.getKey();
            final StorageReference filePath = storageReference.child("message_images").child(pushId + ".jpg");
            Task<Uri> urlTask = filePath.putFile(urlImage).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return filePath.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> thumbTask) {
                    if (thumbTask.isSuccessful()) {
                        final Uri thumbDownloadUrl = thumbTask.getResult();

                        Chat chat = new Chat();
                        chat.setMessage(thumbDownloadUrl.toString());
                        chat.setSeend(false);
                        chat.setTimeNow();
                        chat.setType(Chat.TYPE_MESSAGE.picture);
                        chat.setFrom(currentUser.getUid());

                        Map map = new HashMap();
                        map.put("messages" + "/" + currentUser.getUid() + "/" + friend.getUID() + "/" + pushId, chat);
                        map.put("messages" + "/" + friend.getUID() + "/" + currentUser.getUid() + "/" + pushId, chat);

                        input.setText("");

                        refRut.updateChildren(map, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                if (databaseError != null) {
                                    Log.d("message_image", databaseError.getMessage());
                                }
                            }
                        });

                        Map chats = new HashMap();
                        chats.put("chat/" + currentUser.getUid() + "/" + friend.getUID() + "/message", "Я: изображение");
                        chats.put("chat/" + friend.getUID() + "/" + currentUser.getUid() + "/message", "изображение");
                        chats.put("chat/" + currentUser.getUid() + "/" + friend.getUID() + "/timestamp", Helper.getTimeNow());
                        chats.put("chat/" + friend.getUID() + "/" + currentUser.getUid() + "/timestamp", Helper.getTimeNow());
                        chats.put("chat/" + currentUser.getUid() + "/" + friend.getUID() + "/state", Chat.STATE.none);
                        chats.put("chat/" + currentUser.getUid() + "/" + friend.getUID() + "/seen", true);


                        refRut.updateChildren(chats, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                Helper.pushMessageNotification(new Notifications(currentUser.getUid(), friend.getUID(), Notifications.TYPE_NOTIFI.MESSAGE, "изображение", currentUser.getDisplayName()));
                            }
                        });
                    }
                }
            });
        }

        if (requestCode == MUSIC && resultCode == RESULT_OK) {
            Uri urlMusic = data.getData();





            DatabaseReference userMessagePush = refRut.child("messages").child(currentUser.getUid()).child(friend.getUID()).push();
            final String pushId = userMessagePush.getKey();
            final StorageReference filePath = storageReference.child("message_music").child(pushId + "_::_" + getFileName(urlMusic));

            Task<Uri> urlTask = filePath.putFile(urlMusic).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return filePath.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> thumbTask) {
                    if (thumbTask.isSuccessful()) {
                        final Uri thumbDownloadUrl = thumbTask.getResult();

                        Chat chat = new Chat();
                        chat.setMessage(thumbDownloadUrl.toString());
                        chat.setSeend(false);
                        chat.setTimeNow();
                        chat.setType(Chat.TYPE_MESSAGE.audio);
                        chat.setFrom(currentUser.getUid());

                        Map map = new HashMap();
                        map.put("messages" + "/" + currentUser.getUid() + "/" + friend.getUID() + "/" + pushId, chat);
                        map.put("messages" + "/" + friend.getUID() + "/" + currentUser.getUid() + "/" + pushId, chat);

                        input.setText("");

                        refRut.updateChildren(map, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                if (databaseError != null) {
                                    Log.d("message_music", databaseError.getMessage());
                                }
                            }
                        });

                        Map chats = new HashMap();
                        chats.put("chat/" + currentUser.getUid() + "/" + friend.getUID() + "/message", "Я: аудиофайл");
                        chats.put("chat/" + friend.getUID() + "/" + currentUser.getUid() + "/message", "аудиофайл");
                        chats.put("chat/" + currentUser.getUid() + "/" + friend.getUID() + "/timestamp", Helper.getTimeNow());
                        chats.put("chat/" + friend.getUID() + "/" + currentUser.getUid() + "/timestamp", Helper.getTimeNow());
                        chats.put("chat/" + currentUser.getUid() + "/" + friend.getUID() + "/state", Chat.STATE.none);
                        chats.put("chat/" + currentUser.getUid() + "/" + friend.getUID() + "/seen", true);


                        refRut.updateChildren(chats, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                Helper.pushMessageNotification(new Notifications(currentUser.getUid(),
                                        friend.getUID(), Notifications.TYPE_NOTIFI.MESSAGE, "аудиофайл", currentUser.getDisplayName()));
                            }
                        });
                    }
                }
            });


        }
    }


}
