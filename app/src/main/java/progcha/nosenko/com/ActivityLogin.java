package progcha.nosenko.com;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

public class ActivityLogin extends AppCompatActivity {
    private TextView createAccount;
    private EditText emailEdit, passEdit;
    private Button buttonLogin;
    private ProgressDialog progressDialog;
    private FirebaseAuth mAuth;
    private DatabaseReference user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        createAccount = findViewById(R.id.create_acunt);
        emailEdit = findViewById(R.id.email);
        passEdit = findViewById(R.id.pass);
        buttonLogin = findViewById(R.id.but_log);

        mAuth = FirebaseAuth.getInstance();
        user = FirebaseDatabase.getInstance().getReference().child("users");

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Вход в аккаунт");
        progressDialog.setMessage("Подождите, пожалуйста...");
        progressDialog.setCanceledOnTouchOutside(false);

        createAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ActivityRegistory.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
            }
        });

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(passEdit.getText().toString()) || !TextUtils.isEmpty(emailEdit.getText().toString())) {
                    progressDialog.show();
                    loginUser(emailEdit.getText().toString(), passEdit.getText().toString());
                } else {
                    Toast.makeText(getApplicationContext(), "Заполните все поля...", Toast.LENGTH_SHORT).show();
                }
            }
        });



    }

    private void loginUser(String email, String pass) {

        mAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()){
                    progressDialog.dismiss();
                    String UID = mAuth.getCurrentUser().getUid();
                    String token  = FirebaseInstanceId.getInstance().getToken();
                    user.child(UID).child("tokenId").setValue(token).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            startActivity(new Intent(getApplicationContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                            finish();
                        }
                    });

                } else {
                    progressDialog.hide();
                    Toast.makeText(getApplicationContext(), "Нет такого аккаунта, повторите снова...", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
