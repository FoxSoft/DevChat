package progcha.nosenko.com;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.sdsmdg.tastytoast.TastyToast;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import helper.Helper;
import helper.StatusUser;
import id.zelory.compressor.Compressor;
import model.User;

public class ActivityProfile extends AppCompatActivity {
    private Button butSetStatus, butSetAvatar, butSetDate, butSetGender;
    private TextView status, name, age;
    private CircleImageView avatar;
    private FirebaseUser user;
    private DatabaseReference reference;
    private User userProfile;
    private static final int GALERY_PIK = 1;
    private StorageReference storageReference;
    private ProgressDialog progressDialog;
    private ImageView logo;
    private static final String COLOR_BORDER = "colorBorder";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        avatar = findViewById(R.id.logo);
        name = findViewById(R.id.name);
        status = findViewById(R.id.status);
        age = findViewById(R.id.age);

        butSetAvatar = findViewById(R.id.but_set_avatar);
        butSetStatus = findViewById(R.id.but_set_status);
        butSetDate = findViewById(R.id.but_set_date_rog);
        butSetGender = findViewById(R.id.but_set_gender);

        logo = findViewById(R.id.logo);

        initBut();

        storageReference = FirebaseStorage.getInstance().getReference();
        user = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference().child("users").child(user.getUid());
        reference.keepSynced(true);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                userProfile = new User();
                userProfile.setName(dataSnapshot.child("name").getValue().toString());
                userProfile.setStatus(dataSnapshot.child("status").getValue().toString());
                userProfile.setGender(dataSnapshot.child("gender").getValue().toString() == "жен" ? User.gender.wooman : User.gender.man);
                userProfile.setIdLogo(dataSnapshot.child("idLogo").getValue().toString());
                userProfile.setDateOfBirth(dataSnapshot.child("dateOfBirth").getValue().toString());
                userProfile.setAge(dataSnapshot.child("age").getValue().toString());
                userProfile.setCountFriends((int) dataSnapshot.child("friends").getChildrenCount());

                SharedPreferences sp = getSharedPreferences(COLOR_BORDER, Context.MODE_PRIVATE);
                if (userProfile.getGenderV2() == User.gender.man) {

                    boolean hasVisited = sp.getBoolean("hasVisited", false);

                    // выводим нужную активность
                    SharedPreferences.Editor e = sp.edit();
                    e.putInt("colorBorder", R.color.color_border_man);
                    e.commit(); // не забудьте подтвердить изменения
                } else {
                    SharedPreferences.Editor e = sp.edit();
                    e.putInt("colorBorder", R.color.color_border_wooman);
                    e.commit(); // не забудьте подтвердить изменения
                }

                Log.d("logoUser", userProfile.getIdLogo());
                name.setText(userProfile.getName());
                status.setText(userProfile.getStatus());
                age.setText(userProfile.getAge());

                if (!userProfile.getIdLogo().equals(""))
                    Picasso.get().load(userProfile.getIdLogo()).placeholder(R.drawable.default_avatar).into(logo);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                //Toast.makeText(getApplicationContext(), databaseError.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Загрузка лого");
        progressDialog.setMessage("Подождите, пожалуйста...");
        progressDialog.setCanceledOnTouchOutside(false);

        setSettingPref();
    }

    private void setSettingPref() {
        SharedPreferences sp = getSharedPreferences(COLOR_BORDER, Context.MODE_PRIVATE);
        int hasVisited = sp.getInt("colorBorder", -1);
        if (hasVisited > -1) {
            avatar.setBorderColor(getResources().getColor(hasVisited));
            name.setTextColor(getResources().getColor(hasVisited));
        }
    }

    private void initBut() {
        butSetStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(ActivityProfile.this);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                View v = LayoutInflater.from(getApplicationContext()).inflate(R.layout.editstatus, null);
                final EditText textInputLayout = v.findViewById(R.id.inputStatus);
                final Button butOk = v.findViewById(R.id.butOk);
                butOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        status.setText(textInputLayout.getText().toString());
                        reference = FirebaseDatabase.getInstance().getReference().child("users").child(user.getUid());
                        reference.child("status").setValue(status.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    dialog.dismiss();
                                } else {
                                    TastyToast.makeText(getApplicationContext(), "Нажмите на кнопку снова", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show();
                                }
                            }
                        });
                    }
                });

                dialog.setContentView(v);
                dialog.show();
            }
        });

        butSetAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentGalery = new Intent();
                intentGalery.setType("image/*");
                intentGalery.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intentGalery, "Выбрать лого"), GALERY_PIK);

//                CropImage.activity()
//                        .setGuidelines(CropImageView.Guidelines.ON)
//                        .start(ActivityProfile.this);
            }
        });

        butSetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(ActivityProfile.this);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                View v = LayoutInflater.from(getApplicationContext()).inflate(R.layout.editsdate, null);
                final DatePicker datePicker = v.findViewById(R.id.data);
                final Button butOk = v.findViewById(R.id.butOk);
                butOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String date = datePicker.getDayOfMonth() + "." + datePicker.getMonth() + "." + datePicker.getYear();
                        Time time = new Time();
                        time.setToNow();
                        int age = time.year - datePicker.getYear();
                        reference = FirebaseDatabase.getInstance().getReference().child("users").child(user.getUid());
                        reference.child("age").setValue(String.valueOf(age));
                        reference.child("dateOfBirth").setValue(date).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    dialog.dismiss();
                                    TastyToast.makeText(getApplicationContext(), "Готово", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show();
                                } else {
                                    TastyToast.makeText(getApplicationContext(), "Нажмите на кнопку снова", TastyToast.LENGTH_SHORT, TastyToast.WARNING).show();
                                }
                            }
                        });
                    }
                });

                dialog.setContentView(v);
                dialog.show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALERY_PIK && resultCode == RESULT_OK) {
            Uri urlImage = data.getData();
            CropImage.activity(urlImage)
                    .setAspectRatio(1, 1)
                    .start(ActivityProfile.this);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                progressDialog.show();
                final Uri resultUri = result.getUri();
                File thumbPath = new File(resultUri.getPath());

                Bitmap thumb = null;
                try {
                    thumb = new Compressor(this)
                            .setMaxWidth(400)
                            .setMaxHeight(400)
                            .setQuality(75)
                            .compressToBitmap(thumbPath);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                thumb.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                final byte[] thumbByte = baos.toByteArray();

                final StorageReference filePath = storageReference.child("profile_logo").child(user.getUid() + ".jpg");
                final StorageReference thumbFilePath = storageReference.child("profile_logo").child("thumb").child(user.getUid() + ".jpg");

                Task<Uri> urlTask = filePath.putFile(resultUri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                    @Override
                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                        if (!task.isSuccessful()) {
                            throw task.getException();
                        }
                        return filePath.getDownloadUrl();
                    }
                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                    @Override
                    public void onComplete(@NonNull Task<Uri> task) {
                        if (task.isSuccessful()) {
                            final Uri downloadUri = task.getResult();
                            UploadTask uploadTask = thumbFilePath.putBytes(thumbByte);
                            uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                @Override
                                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                    if (!task.isSuccessful()) {
                                        throw task.getException();
                                    }
                                    return thumbFilePath.getDownloadUrl();
                                }
                            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                @Override
                                public void onComplete(@NonNull Task<Uri> thumbTask) {
                                    if (thumbTask.isSuccessful()) {
                                        final Uri thumbDownloadUrl = thumbTask.getResult();
                                        Map hashMap = new HashMap<>();
                                        hashMap.put("idLogo", downloadUri.toString());
                                        hashMap.put("thumb", thumbDownloadUrl.toString());

                                        reference = FirebaseDatabase.getInstance().getReference().child("users").child(user.getUid());
                                        reference.updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                progressDialog.dismiss();
                                                TastyToast.makeText(getApplicationContext(), "Готово", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS).show();
                                            }
                                        });
                                    }

                                }
                            });
                        } else

                        {
                            progressDialog.hide();
                            TastyToast.makeText(getApplicationContext(), "Ошибка", TastyToast.LENGTH_SHORT, TastyToast.ERROR).show();
                        }
                    }
                });
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

    }

    public static String randomNameLogo() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(10);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Helper.setStatusNet(StatusUser.StatusNet.ONLINE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }
}
