package progcha.nosenko.com;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

class ViewHolderMessage extends RecyclerView.ViewHolder {
    TextView message, title, author, date;

    public ViewHolderMessage(View itemView) {
        super(itemView);
        message = itemView.findViewById(R.id.message);
        date = itemView.findViewById(R.id.date);

    }
}
