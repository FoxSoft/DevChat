package progcha.nosenko.com;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

import helper.StatusUser;
import model.StateUser;
import model.User;

public class ActivityRegistory extends AppCompatActivity {
    private EditText nameEdit, passEdit, emailEdit;
    private Button butReg;
    private FirebaseAuth mAuth;
    private ProgressDialog progressDialog;
    private DatabaseReference database;
    private RadioButton rbW, rbM;
    private User.gender genderUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registory);

        nameEdit = findViewById(R.id.name);
        emailEdit = findViewById(R.id.email);
        passEdit = findViewById(R.id.pass);

        rbM = findViewById(R.id.radioButtonM);
        rbW = findViewById(R.id.radioButtonW);

        inintRB();

        butReg = findViewById(R.id.but_reg);
        butReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!TextUtils.isEmpty(nameEdit.getText().toString()) || !TextUtils.isEmpty(emailEdit.getText().toString()) || !TextUtils.isEmpty(passEdit.getText().toString()))
                    progressDialog.show();
                registerUser(nameEdit.getText().toString(), emailEdit.getText().toString(), passEdit.getText().toString());
            }
        });

        mAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Регистрация");
        progressDialog.setMessage("Подождите, пожалуйста...");
        progressDialog.setCanceledOnTouchOutside(false);

    }

    private void inintRB() {
        if (rbM.isChecked())
            genderUser = User.gender.man;
        else
            genderUser = User.gender.wooman;

        rbW.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                rbW.setChecked(true);
                rbM.setChecked(false);
                compoundButton.setChecked(true);
                genderUser = User.gender.wooman;

            }
        });

        rbM.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                rbM.setChecked(true);
                rbW.setChecked(false);
                compoundButton.setChecked(true);
                genderUser = User.gender.man;

            }
        });
    }

    private void registerUser(final String name, final String email, String pass) {
        mAuth.createUserWithEmailAndPassword(email, pass)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser currentUser = mAuth.getCurrentUser();
                            User userCurrent = new User();
                            userCurrent.setUID(currentUser.getUid());
                            userCurrent.setName(name);
                            userCurrent.setEmail(email);
                            userCurrent.setStatus("Я пользователь DevChat!");
                            userCurrent.setDateOfBirth("");
                            userCurrent.setIdLogo("");
                            userCurrent.setGender(genderUser);
                            userCurrent.setAge("");
                            userCurrent.setThumb("");
                            userCurrent.setStatusNet(StatusUser.StatusNet.ONLINE);
                            userCurrent.setFriends("");
                            userCurrent.setLastSeen("");
                            userCurrent.setTokenId(FirebaseInstanceId.getInstance().getToken());

                            database = FirebaseDatabase.getInstance().getReference().child("users").child(userCurrent.getUID());

                            database.setValue(userCurrent).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        progressDialog.dismiss();
                                        Log.d("regUser", "createUserWithEmail:success");
                                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                        finish();
                                    }
                                }
                            });

                        } else {
                            progressDialog.hide();
                            Log.w("regUser", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(getApplicationContext(), "Не удалось создать аккаунт, повторите снова", Toast.LENGTH_SHORT).show();

                        }
                    }
                });
    }
}
