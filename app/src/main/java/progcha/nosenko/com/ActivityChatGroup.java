package progcha.nosenko.com;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapter.AdapterGroupChat;
import de.hdodenhof.circleimageview.CircleImageView;
import helper.Helper;
import model.Chat;
import model.GroupChat;
import model.Notifications;

public class ActivityChatGroup extends AppCompatActivity {
    private static final int COUNT_LOAD_ITEMS = 30;
    private static final int GALERY_PICKTURE = 1;
    private static final int MUSIC = 2;
    private RecyclerView recyclerView;
    private DatabaseReference refRut;
    private FirebaseUser currentUser;
    private Toolbar toolbar;
    private CircleImageView logo;
    private TextView name;
    private TextView statusNet;
    private SwipeRefreshLayout refreshLayout;
    private ImageView backBut;
    private ImageButton butAdd, butSend;
    private BoomMenuButton bmb;
    private int[] idIcon = new int[]{R.drawable.picture, R.drawable.music, R.drawable.video, R.drawable.arhive, R.drawable.file};
    private String[] titleBut = new String[]{"картинки", "музыка", "видео", "архивы", "файлы"};
    private EditText input;
    private LinearLayout linearLayout;
    private LinearLayoutManager manager;
    private List<Chat> chatList;
    private AdapterGroupChat adapterChat;
    private DatabaseReference refCurrentUser;
    private DatabaseReference updateMessage;
    private String groupChatUID;
    private Map usersGroupChat;
    private DatabaseReference users;
    private DatabaseReference refGroupChatMessage;
    private Map<String, String> usersUID;
    private DatabaseReference refMessage;
    private int currentPage = 1;
    private int itemPos = 0;
    private String lastKey = "";
    private String prevKey = "";
    private Map seensUsers;
    private Map usersThumb;
    private DatabaseReference refChatUsers;
    private String nameCurrentUser;
    private boolean isAddState;
    private View state;
    private TextView titleState;
    private String uidState;
    private boolean add = false;
    private Map booleans = new HashMap();
    private StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_group);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");

        View header = LayoutInflater.from(getApplicationContext()).inflate(R.layout.custom_toolbar, null, false);
        logo = header.findViewById(R.id.logo);
        name = header.findViewById(R.id.name);
        statusNet = header.findViewById(R.id.statusNet);
        backBut = header.findViewById(R.id.backButImg);
        refreshLayout = findViewById(R.id.swipeRef);

        refRut = FirebaseDatabase.getInstance().getReference().getRoot();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();

        toolbar.addView(header);
        setSupportActionBar(toolbar);

        butSend = findViewById(R.id.butSend);
        input = findViewById(R.id.input);
        recyclerView = findViewById(R.id.recycler);
        linearLayout = findViewById(R.id.LL);
        recyclerView.setHasFixedSize(true);
        manager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(manager);


        backBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if (getIntent().getExtras() != null) {
            GroupChat groupChat = new GroupChat();

            groupChatUID = getIntent().getExtras().getString("groupChatUID");
            groupChat.setNameChat(getIntent().getExtras().getString("nameChat"));
            groupChat.setCountUsers(getIntent().getExtras().getInt("countUsers"));

            usersUID = new HashMap<>();
            chatList = new ArrayList<>();
            createCheangeInput();

            usersGroupChat = new HashMap();
            seensUsers = new HashMap();
            usersThumb = new HashMap();

            initBmb();

            updateMessage = refRut.child("ChatGroup");
            storageReference = FirebaseStorage.getInstance().getReference();
            refGroupChatMessage = refRut.child("ChatGroup").child(currentUser.getUid()).child(groupChatUID);
            refCurrentUser = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid());
            refCurrentUser.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    nameCurrentUser = dataSnapshot.child("name").getValue().toString();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            name.setText(groupChat.getNameChat());
            statusNet.setText(groupChat.getCountUsers() + " участника (ов)");

            refGroupChatMessage.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot ds : dataSnapshot.child("users").getChildren()) {
                        if (!ds.getKey().contains(currentUser.getUid())) {
                            usersUID.put(ds.getKey(), ds.getKey());
                            //chatUserMap.put("ChatGroup/" + ds.getKey() + "/" + groupChatUID + "/message", mapMessageFriend);
                        }
                    }
//                    if (!dataSnapshot.hasChild("messages")) {
//                        Map mapMessageMy = new HashMap();
//                        mapMessageMy.put("timestamp", Helper.getTimeNow());
//                        mapMessageMy.put("message", "");
//                        mapMessageMy.put("seen", true);
//                        mapMessageMy.put("state", Chat.STATE.none);
//
//                        Map mapMessageFriend = new HashMap();
//                        mapMessageFriend.put("timestamp", Helper.getTimeNow());
//                        mapMessageFriend.put("message", "");
//                        mapMessageFriend.put("seen", false);
//                        mapMessageFriend.put("state", Chat.STATE.none);
//
//                        Map chatUserMap = new HashMap();
//                        chatUserMap.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/message", mapMessageMy);
//
//                        for (DataSnapshot ds : dataSnapshot.child("users").getChildren()) {
//                            if (!ds.getKey().contains(currentUser.getUid())) {
//                                usersUID.add(ds.getKey());
//                                chatUserMap.put("ChatGroup/" + ds.getKey() + "/" + groupChatUID + "/message", mapMessageFriend);
//                            }
//                        }
//
//                        Log.d("Chat_Log", "update");
//                        refRut.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {
//                            @Override
//                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
//                                if (databaseError != null) {
//                                    Log.d("Chat_Log", databaseError.getMessage());
//                                }
//                            }
//                        });
//                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            refChatUsers = FirebaseDatabase.getInstance().getReference().child("ChatGroup");
            refChatUsers.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot ds : dataSnapshot.getChildren()) {
                        if (!ds.getKey().contains(currentUser.getUid()))
                            adapterChat.updateSeeItem(ds.child(groupChatUID).child("seen").getValue(Boolean.class));
                        // Log.d("refChatUsers", ds.child(groupChatUID).child("seen").getValue(Boolean.class)+"");
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


        }

        butSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Helper.isOnline3(getApplicationContext()))
                    sendMessage();
            }
        });

        adapterChat = new AdapterGroupChat(chatList, getApplicationContext(), usersThumb);
        adapterChat.setHasStableIds(true);
        recyclerView.setAdapter(adapterChat);

        loadMessage();
        createStateUser();
        getStateUser();

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage++;
                itemPos = 0;
                loadMoreMessage();
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        getMessageNotSeenFriend();

        users = FirebaseDatabase.getInstance().getReference().child("users");
        users.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    //  usersGroupChat.put(++, ds.child("friends"))
                    usersThumb.put(ds.getKey(), ds.child("thumb").getValue().toString());
                    // Log.d("friends", ds.child("thumb").getValue().toString());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    private void createStateUser() {
        state = LayoutInflater.from(getApplicationContext()).inflate(R.layout.layout_input_user, null);
        titleState = state.findViewById(R.id.title);

    }

    private void addStateUser() {
        isAddState = true;
        titleState.setText(uidState + " набирает...");
        linearLayout.addView(state, 0, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
    }

    private void delStateUser() {
        isAddState = false;
        linearLayout.removeView(state);
    }

    private void createCheangeInput() {
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d("inputState", "beforeTextChanged");

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int q = 0;
                if (!charSequence.equals("")) {
                    if (i != q)
                        updateStateUser(Chat.STATE.write);
                    q = i;
                }
                if (i == 0) {
                    updateStateUser(Chat.STATE.none);
                }

                Log.d("inputState1", "" + i);
            }

            @Override
            public void afterTextChanged(Editable editable) {

                Log.d("inputState", "afterTextChanged");
            }
        });
    }

    private void updateStateUser(Chat.STATE state) {
        Map chat = new HashMap();
        chat.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/state", state);

        refRut.updateChildren(chat, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

            }
        });
    }

    private void getStateUser() {
        updateMessage.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                Chat.STATE state = Chat.STATE.none;
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Log.d("getStateUser", ds.child(groupChatUID).child("state").getValue().toString());
                    if (!ds.getKey().contains(currentUser.getUid())) {
                        state = ds.child(groupChatUID).child("state").getValue(Chat.STATE.class);
                        uidState = ds.getKey();

                        booleans.put(state.equals(Chat.STATE.write), ds.child(groupChatUID).child("nameUser").getValue().toString());
                    }
                }


                if (booleans.containsKey(true)) {
                    try {
                        uidState = (String) booleans.get(true);
                        addStateUser();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        delStateUser();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

//                if (!add) {
//                    try {
//                        delStateUser();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                } else try {
//                    addStateUser();
//
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                booleans.clear();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void initBmb() {

        bmb = findViewById(R.id.bmb);
        bmb.setBackgroundEffect(true);

        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            TextInsideCircleButton.Builder builder = new TextInsideCircleButton.Builder()
                    .normalImageRes(idIcon[i])
                    // .highlightedText(titleBut[i])
                    .shadowEffect(true)
                    .shadowOffsetX(15)
                    .buttonCornerRadius(20)
                    .normalColor(getResources().getColor(R.color.colorPrimaryDark))
                    .highlightedColor(getResources().getColor(R.color.colorAccent))
                    .shadowOffsetY(5)
                    .shadowRadius(Util.dp2px(10))
                    .shadowCornerRadius(Util.dp2px(20))
                    .shadowColor(Color.parseColor("#AA000000"))
                    .imagePadding(new Rect(5, 5, 5, 5))
                    .isRound(false);

            bmb.addBuilder(builder);
            bmb.setOnBoomListener(new OnBoomListener() {
                @Override
                public void onClicked(int index, BoomButton boomButton) {
                    Log.d("clickBoom", "onClicked" + index);
                    Intent intent = new Intent();
                    switch (index) {
                        case 0:

                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Выбрать фото"), GALERY_PICKTURE);
                            break;

                        case 1:
                            intent.setType("audio/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(intent, "Выбрать музыку"), MUSIC);
                            break;
                    }
                }

                @Override
                public void onBackgroundClick() {

                }

                @Override
                public void onBoomWillHide() {

                }

                @Override
                public void onBoomDidHide() {

                }

                @Override
                public void onBoomWillShow() {

                }

                @Override
                public void onBoomDidShow() {

                }
            });
        }
    }

    private void sendMessage() {

        if (!TextUtils.isEmpty(input.getText().toString())) {
            DatabaseReference messageRef = refRut.child("ChatGroup").child(currentUser.getUid()).child(groupChatUID).child("messages").push();
            String pushId = messageRef.getKey();

            final Chat chat = new Chat();
            chat.setMessage(input.getText().toString());
            chat.setSeend(false);
            chat.setTimeNow();
            chat.setType(Chat.TYPE_MESSAGE.text);
            chat.setFrom(currentUser.getUid());
            chat.setSeen(false);

            Map map = new HashMap();
            map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/messages/" + pushId, chat);

            map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/lastMessage", chat.getMessage());
            map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/nameUserLast", "Я");
            map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/timestamp", Helper.getTimeNow());
            map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/state", Chat.STATE.none);
            map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/seen", true);

            for (Map.Entry<String, String> entry : usersUID.entrySet()) {
                map.put("ChatGroup/" + entry.getKey() + "/" + groupChatUID + "/messages/" + pushId, chat);

                map.put("ChatGroup/" + entry.getKey() + "/" + groupChatUID + "/lastMessage", chat.getMessage());
                map.put("ChatGroup/" + entry.getKey() + "/" + groupChatUID + "/nameUserLast", nameCurrentUser);
                map.put("ChatGroup/" + entry.getKey() + "/" + groupChatUID + "/timestamp", Helper.getTimeNow());
            }


            input.setText("");
            refRut.updateChildren(map, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                    DatabaseReference chatMessage = refRut.child("ChatGroup").child(currentUser.getUid()).child(groupChatUID).child("messages").push();
                    for (Map.Entry<String, String> entry : usersUID.entrySet()) {
                        // System.out.println("ID =  " + entry.getKey() + " День недели = " + entry.getValue());
                        Helper.pushMessageNotification(new Notifications(currentUser.getUid(), entry.getKey(),
                                Notifications.TYPE_NOTIFI.MESSAGE, chat.getMessage(), ""));
                    }
                }
            });
        }
    }

    private void loadMessage() {
        refMessage = refRut.child("ChatGroup").child(currentUser.getUid()).child(groupChatUID).child("messages");
        Query messageQuery = refMessage.limitToLast(currentPage * COUNT_LOAD_ITEMS);
        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Chat chat = dataSnapshot.getValue(Chat.class);
                itemPos++;
                if (itemPos == 1) {
                    String messageKey = dataSnapshot.getKey();
                    lastKey = messageKey;
                    prevKey = messageKey;
                }

                Log.d("type_message", chat.getType().toString());
                chatList.add(chat);

                adapterChat.notifyDataSetChanged();
                recyclerView.scrollToPosition(chatList.size() - 1);
                refreshLayout.setRefreshing(false);

                Log.d("chatList", chatList.toString());
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void loadMoreMessage() {
        DatabaseReference refMessage = refRut.child("ChatGroup").child(currentUser.getUid()).child(groupChatUID).child("messages");
        Query messageQuery = refMessage.orderByKey().endAt(lastKey).limitToLast(10);
        messageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Chat chat = dataSnapshot.getValue(Chat.class);
                String messageKey = dataSnapshot.getKey();

                if (!prevKey.contains(messageKey)) {
                    chatList.add(itemPos++, chat);
                } else {
                    prevKey = lastKey;
                }

                if (itemPos == 1) {
                    lastKey = messageKey;
                }
                adapterChat.notifyDataSetChanged();
                refreshLayout.setRefreshing(false);
                manager.scrollToPositionWithOffset(10, 0);

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateChatSeen(boolean seen) {
        Map chat = new HashMap();
        chat.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/seen", seen);

        refRut.updateChildren(chat, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

            }
        });
    }

    private void getMessageNotSeenFriend() {
        updateMessage.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                //  if (!dataSnapshot.child("from").getValue().toString().equals(currentUser.getUid()) & !dataSnapshot.child("seen").getValue(Boolean.class)) {
                String UID = dataSnapshot.getKey();
                //Log.d("getMessageNotSeenFriend", dataSnapshot.child(groupChatUID).child("messages").getKey());
                for (DataSnapshot ds : dataSnapshot.child(groupChatUID).child("messages").getChildren()) {
                    if (!UID.contains(currentUser.getUid()) & !ds.child("seen").getValue(Boolean.class)) {
                        Log.d("getMessageNotSeenFriend", "key: " + UID + " " + ds.getKey());
                        seensUsers.put(UID + "/" + groupChatUID + "/messages/" + ds.getKey() + "/seen", true);
                    }

                }

                //  }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Log.d("leesen onChildChanged", "myMess" + dataSnapshot.getKey());
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void updateMessageUsers() {
        if (seensUsers != null)
            updateMessage.updateChildren(seensUsers, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                }
            });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        updateChatSeen(false);
        updateMessageUsers();
    }

    @Override
    protected void onStop() {
        super.onStop();
        updateChatSeen(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateChatSeen(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALERY_PICKTURE && resultCode == RESULT_OK) {
            Uri urlImage = data.getData();
            DatabaseReference userMessagePush = refRut.child("ChatGroup").child(currentUser.getUid()).child(groupChatUID).child("messages").push();
            final String pushId = userMessagePush.getKey();
            final StorageReference filePath = storageReference.child("ChatGroup").child("message_images").child(pushId + ".jpg");
            Task<Uri> urlTask = filePath.putFile(urlImage).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return filePath.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> thumbTask) {
                    if (thumbTask.isSuccessful()) {
                        final Uri thumbDownloadUrl = thumbTask.getResult();

                        Chat chat = new Chat();
                        chat.setMessage(thumbDownloadUrl.toString());
                        chat.setSeend(false);
                        chat.setTimeNow();
                        chat.setType(Chat.TYPE_MESSAGE.picture);
                        chat.setFrom(currentUser.getUid());

                        Map map = new HashMap();
                        map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/messages" + "/" + pushId, chat);
                        map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/lastMessage", "картинка");
                        map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/nameUserLast", "Я");
                        map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/timestamp", Helper.getTimeNow());
                        map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/state", Chat.STATE.none);
                        map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/seen", true);

                        for (Map.Entry<String, String> entry : usersUID.entrySet()) {
                            map.put("ChatGroup/" + entry.getKey() + "/" + groupChatUID + "/messages/" + pushId, chat);

                            map.put("ChatGroup/" + entry.getKey() + "/" + groupChatUID + "/lastMessage", "картинка");
                            map.put("ChatGroup/" + entry.getKey() + "/" + groupChatUID + "/nameUserLast", nameCurrentUser);
                            map.put("ChatGroup/" + entry.getKey() + "/" + groupChatUID + "/timestamp", Helper.getTimeNow());
                        }
                        // map.put("messages" + "/" + friend.getUID() + "/" + currentUser.getUid() + "/" + pushId, chat);

                        input.setText("");

                        refRut.updateChildren(map, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                if (databaseError != null) {
                                    Log.d("message_image", databaseError.getMessage());
                                    for (Map.Entry<String, String> entry : usersUID.entrySet()) {
                                        Helper.pushMessageNotification(new Notifications(currentUser.getUid(),
                                                entry.getKey(), Notifications.TYPE_NOTIFI.MESSAGE, "изображение", nameCurrentUser));
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }

        if (requestCode == MUSIC && resultCode == RESULT_OK) {
            Uri urlMusic = data.getData();

            DatabaseReference userMessagePush = refRut.child("ChatGroup").child(currentUser.getUid()).child(groupChatUID).child("messages").push();
            final String pushId = userMessagePush.getKey();
            final StorageReference filePath = storageReference.child("ChatGroup").child("message_music").child(pushId + "_::_" + Helper.getFileName(urlMusic, getApplicationContext()));

            Task<Uri> urlTask = filePath.putFile(urlMusic).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()) {
                        throw task.getException();
                    }
                    return filePath.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> thumbTask) {
                    if (thumbTask.isSuccessful()) {
                        final Uri thumbDownloadUrl = thumbTask.getResult();

                        Chat chat = new Chat();
                        chat.setMessage(thumbDownloadUrl.toString());
                        chat.setSeend(false);
                        chat.setTimeNow();
                        chat.setType(Chat.TYPE_MESSAGE.audio);
                        chat.setFrom(currentUser.getUid());

                        Map map = new HashMap();
                        map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/messages" + "/" + pushId, chat);
                        map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/lastMessage", "аудиофайл");
                        map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/nameUserLast", "Я");
                        map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/timestamp", Helper.getTimeNow());
                        map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/state", Chat.STATE.none);
                        map.put("ChatGroup/" + currentUser.getUid() + "/" + groupChatUID + "/seen", true);

                        for (Map.Entry<String, String> entry : usersUID.entrySet()) {
                            map.put("ChatGroup/" + entry.getKey() + "/" + groupChatUID + "/messages/" + pushId, chat);

                            map.put("ChatGroup/" + entry.getKey() + "/" + groupChatUID + "/lastMessage", "аудиофайл");
                            map.put("ChatGroup/" + entry.getKey() + "/" + groupChatUID + "/nameUserLast", nameCurrentUser);
                            map.put("ChatGroup/" + entry.getKey() + "/" + groupChatUID + "/timestamp", Helper.getTimeNow());
                        }

                        input.setText("");

                        refRut.updateChildren(map, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                if (databaseError != null) {
                                    Log.d("message_music", databaseError.getMessage());
                                    for (Map.Entry<String, String> entry : usersUID.entrySet()) {
                                        Helper.pushMessageNotification(new Notifications(currentUser.getUid(),
                                                entry.getKey(), Notifications.TYPE_NOTIFI.MESSAGE, "аудиофайл", nameCurrentUser));
                                    }
                                }
                            }
                        });


                    }
                }
            });


        }
    }
}
