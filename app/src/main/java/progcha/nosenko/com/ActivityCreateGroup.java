package progcha.nosenko.com;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapter.AdapterSelectUserGroup;
import helper.Helper;
import model.Chat;
import model.GroupChat;
import model.Notifications;
import model.SelectUser;

public class ActivityCreateGroup extends AppCompatActivity {
    private RecyclerView recyclerView;
    private AdapterSelectUserGroup adapterSelectUserGroup;
    private List<SelectUser> users;
    private Button button;
    private EditText nameGroup;
    private LinearLayoutManager manager;
    private FirebaseUser currentUser;
    private DatabaseReference myFriends;
    private Map<String, SelectUser> friendMyList;
    private DatabaseReference userRef, refRoot;
    private Map chatGroupMap;
    private GroupChat groupChat;
    private DatabaseReference notifi;
    private String nameCurrenUser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);

        recyclerView = findViewById(R.id.recycler);
        button = findViewById(R.id.butCreate);
        nameGroup = findViewById(R.id.editNameGroup);

        recyclerView.setHasFixedSize(true);
        manager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(manager);

        users = new ArrayList<>();
        adapterSelectUserGroup = new AdapterSelectUserGroup(users, getApplicationContext());
        recyclerView.setAdapter(adapterSelectUserGroup);

        chatGroupMap = new HashMap();
        groupChat = new GroupChat();

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        myFriends = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid()).child("friends");
        userRef = FirebaseDatabase.getInstance().getReference().child("users");
        notifi = FirebaseDatabase.getInstance().getReference().child("notifications");
        refRoot = FirebaseDatabase.getInstance().getReference().getRoot();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String key = refRoot.push().getKey();
                Map map = new HashMap();
                final Map mapNames = new HashMap();
                final List<SelectUser> list = adapterSelectUserGroup.getSelected();

                for (int i = 0; i < list.size(); i++) {
                    map.put(list.get(i).getUID(), list.get(i).getState());

                    chatGroupMap.put("ChatGroup/" + list.get(i).getUID() + "/" + key, groupChat);
                    mapNames.put("ChatGroup/" + list.get(i).getUID() + "/" + key + "/nameUser", list.get(i).getName());
                }

                map.put(currentUser.getUid(), SelectUser.STATE_USER_GROUP_CHAT.added);

                groupChat.setUsers(map);
                groupChat.setAuthorGroupUID(currentUser.getUid());
                groupChat.setCountUsers(users.size() + 1);
                groupChat.setNameChat(nameGroup.getText().toString());
                groupChat.setLastMessage("");
                groupChat.setNameUserLast("");
                groupChat.setSeen(false);
                groupChat.setState(Chat.STATE.none);
                groupChat.setLastMessage("");
                groupChat.setThumb("");
                groupChat.setTimestamp(Helper.getTimeNow());

                chatGroupMap.put("ChatGroup/" + currentUser.getUid() + "/" + key, groupChat);
                mapNames.put("ChatGroup/" + currentUser.getUid() + "/" + key + "/nameUser", nameCurrenUser);

                refRoot.updateChildren(chatGroupMap, new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                        refRoot.updateChildren(mapNames, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                for (int i = 0; i < list.size(); i++) {
                                    Notifications notifications = new Notifications(currentUser.getUid(), list.get(i).getUID(), Notifications.TYPE_NOTIFI.REQUEST,
                                            currentUser.getDisplayName() + " добавил вас в беседу \"" + groupChat.getNameChat() + "\"",
                                            currentUser.getDisplayName() + " добавил вас в беседу \"" + groupChat.getNameChat() + "\"");
                                    notifi.child(list.get(i).getUID()).push().setValue(notifications).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            finish();
                                        }
                                    });
                                }
                            }
                        });
                    }
                });
            }
        });

        getMyFriends();
    }

    private void getMyFriends() {
        friendMyList = new HashMap<>();
        myFriends.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    final SelectUser user = new SelectUser();
                    user.setState(SelectUser.STATE_USER_GROUP_CHAT.added);
                    user.setUID(ds.getKey());

                    userRef.child(ds.getKey()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            users.clear();
                            user.setName(dataSnapshot.child("name").getValue().toString());

                            friendMyList.put(user.getUID(), user);

                            users.addAll((Collection<? extends SelectUser>) friendMyList.values());
                            adapterSelectUserGroup.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                    Log.d("counterFriends", ds.getKey() + " s " + friendMyList.size());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        userRef.child(currentUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                nameCurrenUser = dataSnapshot.child("name"). getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
