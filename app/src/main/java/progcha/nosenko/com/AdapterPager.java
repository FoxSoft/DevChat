package progcha.nosenko.com;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

class AdapterPager extends FragmentPagerAdapter {
    public AdapterPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                FragmentRequest fragmentRequest = new FragmentRequest();
                return fragmentRequest;
            case 1:
                FragmentChat fragmentChat = new FragmentChat();
                return fragmentChat;
            case 2:
                FragmentFrieds fragmentFrieds = new FragmentFrieds();
                return fragmentFrieds;
            default:
                FragmentChat fragmentChat2 = new FragmentChat();
                return fragmentChat2;
        }
    }

    @Override
    public int getCount() {
        return 3;

    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position)
        {
            case 0:
                return "Запрос";
            case 1:
                return "Чат";
            case 2:
                return "Друзья";
            default:
                return null;
        }
    }
}
