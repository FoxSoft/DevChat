package progcha.nosenko.com;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.nex3z.notificationbadge.NotificationBadge;
import com.yalantis.contextmenu.lib.ContextMenuDialogFragment;
import com.yalantis.contextmenu.lib.MenuObject;
import com.yalantis.contextmenu.lib.MenuParams;
import com.yalantis.contextmenu.lib.interfaces.OnMenuItemClickListener;
import com.yalantis.contextmenu.lib.interfaces.OnMenuItemLongClickListener;

import java.util.ArrayList;
import java.util.List;

import adapter.AdapterPage;
import helper.Helper;
import helper.StatusUser;
import model.StateUser;

import static helper.StatusUser.StatusNet.OFLINE;
import static helper.StatusUser.StatusNet.ONLINE;

public class MainActivity extends AppCompatActivity implements MenuItem.OnMenuItemClickListener, OnMenuItemLongClickListener {

    private FirebaseAuth mAuth;
    private Toolbar toolbar;
    private ViewPager viewPager;
    private AdapterPage adapterPager;
    private TabLayout tabLayout;
    private DatabaseReference status, countRec;
    private FirebaseUser currentUser;
    private Thread ThreadStatusNet;
    private HelperTabTitle helperTabTitle1, helperTabTitle2, helperTabTitle3;
    private Thread threadCountRec;
    private FragmentChat fragmentChat;
    private ContextMenuDialogFragment mMenuDialogFragment;
    private FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getSupportFragmentManager();
        initMenuFragment();

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("DevChat");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white_orig));
        setSupportActionBar(toolbar);


        Log.d("displayMetric", Helper.getSizeDisplay(getWindowManager(), Helper.DISPLAY.width) + "");

        countRec = FirebaseDatabase.getInstance().getReference();

        viewPager = findViewById(R.id.pager);
        setupViewPager(viewPager);

        tabLayout = findViewById(R.id.tabL);
        tabLayout.setupWithViewPager(viewPager);

//        Log.d("windth tablayout", Helper.getSizeDisplay(getWindowManager(), Helper.DISPLAY.width)+"");
//        if (Helper.getSizeDisplay(getWindowManager(), Helper.DISPLAY.width) <= 720) {
//            tabLayout.getTabAt(0).setCustomView(R.layout.title_tab_720);
//            tabLayout.getTabAt(1).setCustomView(R.layout.title_tab_720);
//            tabLayout.getTabAt(2).setCustomView(R.layout.title_tab_720);
//        } else {
        tabLayout.getTabAt(0).setCustomView(R.layout.title_tab);
        tabLayout.getTabAt(1).setCustomView(R.layout.title_tab);
        tabLayout.getTabAt(2).setCustomView(R.layout.title_tab);


        createHeader();

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
    }

    private void initMenuFragment() {
        MenuParams menuParams = new MenuParams();
        menuParams.setActionBarSize((int) getResources().getDimension(R.dimen.tool_bar_height));
        menuParams.setMenuObjects(getMenuObjects());
        menuParams.setClosableOutside(false);
        mMenuDialogFragment = ContextMenuDialogFragment.newInstance(menuParams);
        mMenuDialogFragment.setItemClickListener(new OnMenuItemClickListener() {
            @Override
            public void onMenuItemClick(View clickedView, int position) {
                switch (position) {
                    case 0:
                        break;

                    case 1:
                        startActivity(new Intent(getApplicationContext(), ActivityProfile.class));
                        finish();
                        break;

                    case 2:
                        startActivity(new Intent(getApplicationContext(), ActivityUsers.class));
                        break;

                    case 3:
                        startActivity(new Intent(getApplicationContext(), ActivityCreateGroup.class));
                        break;

                    case 4:
                        FirebaseAuth.getInstance().signOut();
                        startLogin();
                        break;

                    case 5:
                        startActivity(new Intent(getApplicationContext(), ActivityChatGroup.class));
                        break;

                }

            }
        });
        mMenuDialogFragment.setItemLongClickListener(this);
    }

    private List<MenuObject> getMenuObjects() {
        // You can use any [resource, bitmap, drawable, color] as image:
        // item.setResource(...)
        // item.setBitmap(...)
        // item.setDrawable(...)
        // item.setColor(...)
        // You can set image ScaleType:
        // item.setScaleType(ScaleType.FIT_XY)
        // You can use any [resource, drawable, color] as background:
        // item.setBgResource(...)
        // item.setBgDrawable(...)
        // item.setBgColor(...)
        // You can use any [color] as text color:
        // item.setTextColor(...)
        // You can set any [color] as divider color:
        // item.setDividerColor(...)

        List<MenuObject> menuObjects = new ArrayList<>();

        MenuObject close = new MenuObject();
        close.setResource(R.drawable.cancel);
        close.setBgResource(R.drawable.back_menu);


        MenuObject profile = new MenuObject("Профиль");
        profile.setResource(R.drawable.profile);
        profile.setBgResource(R.drawable.back_menu);

        MenuObject users = new MenuObject("Пользователи");
        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.users);
        users.setBitmap(b);
        users.setBgResource(R.drawable.back_menu);

        MenuObject logOut = new MenuObject("Выйти");
        BitmapDrawable bd = new BitmapDrawable(getResources(), BitmapFactory.decodeResource(getResources(), R.drawable.logout));
        logOut.setDrawable(bd);
        logOut.setBgResource(R.drawable.back_menu);

        MenuObject createGroupMessage = new MenuObject("Создать бседу");
        createGroupMessage.setResource(R.drawable.group_message);
        createGroupMessage.setBgResource(R.drawable.back_menu);

        MenuObject createGroup = new MenuObject("test group chat");
        createGroupMessage.setResource(R.drawable.group_message);
        createGroupMessage.setBgResource(R.drawable.back_menu);


        menuObjects.add(close);
        menuObjects.add(profile);
        menuObjects.add(users);
        menuObjects.add(createGroupMessage);
        menuObjects.add(logOut);
        menuObjects.add(createGroup);

        return menuObjects;
    }

    private void setupViewPager(ViewPager viewPager) {
        adapterPager = new AdapterPage(getSupportFragmentManager());
        adapterPager.addFragment(new FragmentRequest(), "Запросы");
        adapterPager.addFragment(new FragmentChat(), "Диалоги");
        adapterPager.addFragment(new FragmentFrieds(), "Друзья");
        viewPager.setAdapter(adapterPager);
    }

    private void createHeader() {
        helperTabTitle1 = new HelperTabTitle(tabLayout.getTabAt(0).getCustomView());
        helperTabTitle1.setNumberBadge(0);
        helperTabTitle1.setImg(R.drawable.requests);
        helperTabTitle1.setTitle("Запросы");

        helperTabTitle2 = new HelperTabTitle(tabLayout.getTabAt(1).getCustomView());
        helperTabTitle2.setImg(R.drawable.dialogs);
        helperTabTitle2.setNumberBadge(0);
        helperTabTitle2.setTitle("Диалоги");

        helperTabTitle3 = new HelperTabTitle(tabLayout.getTabAt(2).getCustomView());
        helperTabTitle3.setImg(R.drawable.friends);
        helperTabTitle3.setNumberBadge(0);
        helperTabTitle3.setTitle("Друзья");
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (currentUser == null)
            startLogin();
        else {
            setStatusNet(ONLINE);
            setBandgeHeader();
        }
    }

    private void setBandgeHeader() {
        countRec.child("users").child(currentUser.getUid()).child("friends").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                Log.d("countFriends", dataSnapshot.getChildrenCount()+"");
                helperTabTitle3.setNumberBadge((int) dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        countRec.child("chat").child(currentUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                helperTabTitle2.setNumberBadge((int) dataSnapshot.getChildrenCount());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        countRec.child("request").child(currentUser.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int i = 0;
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    if (!ds.child("requestType").getValue(StateUser.REQUEST_TYPE.class).equals(StateUser.REQUEST_TYPE.friend))
                        i++;
                }
                helperTabTitle1.setNumberBadge(i);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void setCountRec(final boolean isComplete) {

        threadCountRec = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                    Log.d("threadCountRec", "Привет из потока " + Thread.currentThread().getName());

                    if (isComplete) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Log.d("threadCountRec", "Привет из потока runOnUiThread");
                            }
                        });
                        threadCountRec.interrupt();

                        Log.d("threadCountRec", "сдохни " + Thread.currentThread().getName());
                    } else {

                    }

                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        });

        threadCountRec.start();
    }

    private void startLogin() {
        startActivity(new Intent(this, ActivityLogin.class));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {

            }
            //displayChat();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_activity, menu);

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.context_menu:
                if (fragmentManager.findFragmentByTag(ContextMenuDialogFragment.TAG) == null) {
                    mMenuDialogFragment.show(fragmentManager, ContextMenuDialogFragment.TAG);
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        setStatusNet(OFLINE);
        Helper.setLastSeen();
    }

    private void setStatusNet(StatusUser.StatusNet stat) {
        DatabaseReference statusNet = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid()).child("statusNet");
        statusNet.setValue(stat).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                }
            }
        });
    }

    private void setLastSeen() {
        DatabaseReference last = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid()).child("lastSeen");
        last.setValue(ServerValue.TIMESTAMP).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {

                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        setStatusNet(ONLINE);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // setStatusNet(ONLINE);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        //setStatusNet(ONLINE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        setStatusNet(OFLINE);
//        ThreadStatusNet = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                while (!Thread.currentThread().isInterrupted()) {
//                    try {
//                        Thread.sleep(300000);
//                    } catch (InterruptedException e) {
//                        return;
//                    }
//
//                    Log.d("ThreadStatusNet", "Привет из потока " + Thread.currentThread().getNameChat());
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            setStatusNet(OFLINE);
//                            Helper.setLastSeen();
//                            ThreadStatusNet.interrupt();
//                           // ThreadStatusNet.destroy();
//                            Log.d("ThreadStatusNet", "Привет из потока runOnUiThread");
//                        }
//                    });
//
//
//                    Log.d("ThreadStatusNet", "сдохни " + Thread.currentThread().getNameChat());
//                }
//            }
//        });
//
//        ThreadStatusNet.start();

    }

    @Override
    public void onBackPressed() {
        if (mMenuDialogFragment != null && mMenuDialogFragment.isAdded()) {
            mMenuDialogFragment.dismiss();
        } else {
            finish();
        }
    }


    public void onMenuItemClick(View clickedView, int position) {


    }


    @Override
    public void onMenuItemLongClick(View clickedView, int position) {
        // Toast.makeText(this, "Long clicked on position: " + position, Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 1:
                Toast.makeText(this, "lol", Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }

    public class HelperTabTitle {
        private NotificationBadge badge;
        private int click = 0;
        private ImageView img;
        private int number;
        private RelativeLayout relativeLayout;
        private TextView title;
        private View view;

        public HelperTabTitle(View v) {
            view = v;
            badge = view.findViewById(R.id.badge);
            title = view.findViewById(R.id.nameStation);
            img = view.findViewById(R.id.icon);
            relativeLayout = view.findViewById(R.id.RL);
        }

        public void setNumberBadge(int n) {
            badge.setNumber(n);
            number = n;
        }

        public void incNumerBadge() {
            NotificationBadge notificationBadge = badge;
            int i = number + 1;
            number = i;
            notificationBadge.setNumber(i);
        }

        public void setImg(int idImg) {
            img.setImageResource(idImg);
        }

        public void setTitle(String s) {
            title.setText(s);
        }
    }
}
