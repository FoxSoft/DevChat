package progcha.nosenko.com;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.TextInsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.OnBoomListener;
import com.nightonke.boommenu.Util;

import model.Users;

public class ActivityTest extends AppCompatActivity {
    private DatabaseReference reference;
    private TextView textView;
    private String body;
    private String title;
    private String UID;
    private String name;
    private String thumb;
    private BoomMenuButton bmb;
    private int[] idIcon = new int[]{R.drawable.picture, R.drawable.music, R.drawable.video, R.drawable.arhive, R.drawable.file};
    private int[] idColor = new int[]{};
    private String[] titleBut = new String[]{"картинки", "музыка", "видео", "архивы", "файлы"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        textView = findViewById(R.id.t);

        bmb = findViewById(R.id.bmb);
        bmb.setBackgroundEffect(true);
        bmb.setBackgroundDrawable(getResources().getDrawable(R.drawable.add));

        for (int i = 0; i < bmb.getPiecePlaceEnum().pieceNumber(); i++) {
            TextInsideCircleButton.Builder builder = new TextInsideCircleButton.Builder()
                    .normalImageRes(idIcon[i])
                   // .highlightedText(titleBut[i])
                    .shadowEffect(true)
                    .shadowOffsetX(15)
                    .buttonCornerRadius(20)
                    .normalColor(getResources().getColor(R.color.colorPrimaryDark))
                    .highlightedColor(getResources().getColor(R.color.colorAccent))
                    .shadowOffsetY(5)
                    .shadowRadius(Util.dp2px(10))
                    .shadowCornerRadius(Util.dp2px(20))
                    .shadowColor(Color.parseColor("#AA000000"))
                    .imagePadding(new Rect(5,5,5,5))
                    .isRound(false);

            bmb.addBuilder(builder);
            bmb.setOnBoomListener(new OnBoomListener() {
                @Override
                public void onClicked(int index, BoomButton boomButton) {
                    Log.d("clickBoom", "onClicked" + index);
                }

                @Override
                public void onBackgroundClick() {

                }

                @Override
                public void onBoomWillHide() {

                }

                @Override
                public void onBoomDidHide() {

                }

                @Override
                public void onBoomWillShow() {

                }

                @Override
                public void onBoomDidShow() {

                }
            });
        }



    }



    @Override
    protected void onPause() {
        super.onPause();

    }

}
