package progcha.nosenko.com;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.pm.ApplicationInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import adapter.AdapterUsers;
import model.Users;

import static helper.StatusUser.StatusNet.OFLINE;

public class ActivityUsers extends AppCompatActivity {
    private RecyclerView recyclerView;
    private DatabaseReference reference;
    private List<Users> users;
    private AdapterUsers adapterUsers;
    private ProgressDialog progressDialog;
    private Thread allStThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        recyclerView = findViewById(R.id.recycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

        reference = FirebaseDatabase.getInstance().getReference().child("users");

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Поиск пользователей");
        progressDialog.setMessage("Подождите, пожалуйста...");
        progressDialog.setCanceledOnTouchOutside(false);
    }

    @Override
    protected void onStart() {
        super.onStart();

//        query.addChildEventListener(new ChildEventListener() {
//            @Override
//            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//                adapterUsers.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//                adapterUsers.notifyDataSetChanged();
//            }
//
//            @Override
//            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
//
//            }
//
//            @Override
//            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });

      //  FirebaseRecyclerOptions<Users> options = new FirebaseRecyclerOptions.Builder<Users>().setQuery(query, Users.class).build();
//        adapterUsers = new AdapterUsers(options, getApplicationContext());
//        recyclerView.setAdapter(adapterUsers);


        progressDialog.show();
        adapterUsers = new AdapterUsers(getApplicationContext(), reference);
        recyclerView.setAdapter(adapterUsers);

        getSucces();
    }

    private void getSucces() {
        allStThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                    Log.d("myThreadAllS", "Привет из потока " + Thread.currentThread().getName());

                    if (adapterUsers.isIdSet()) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    progressDialog.dismiss();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Log.d("myThreadAllS", "Привет из потока runOnUiThread");
                            }
                        });
                        allStThread.interrupt();

                        Log.d("myThreadAllS", "сдохни " + Thread.currentThread().getName());
                    } else {
                        Log.d("myThreadAllS", "живу " + Thread.currentThread().getName());
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                            e.fillInStackTrace();
                        }
                    }
                }
            }
        });

        allStThread.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapterUsers.cleanupListener();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


}
