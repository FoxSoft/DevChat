package progcha.nosenko.com;


import adapter.AdapterRequest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.*;
import helper.StatusUser;
import model.Request;
import model.StateUser;

import java.util.*;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentRequest extends Fragment {


    private View view;
    private RecyclerView recyclerView;
    private AdapterRequest adapterRequest;
    private List<Request> requests;
    private DatabaseReference requestRef;
    private FirebaseUser currentUser;
    private DatabaseReference userRequest;

    public FragmentRequest() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_request, container, false);

        recyclerView = view.findViewById(R.id.rec);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        requests = new ArrayList<>();

        adapterRequest = new AdapterRequest(getContext(), requests);
        recyclerView.setAdapter(adapterRequest);

        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        requestRef = FirebaseDatabase.getInstance().getReference().child("request");
        userRequest = FirebaseDatabase.getInstance().getReference().child("users");

        getRequests();

        return view;
    }

    private void getRequests() {
        requestRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final Map mapRequest = new HashMap();

                for (DataSnapshot ds : dataSnapshot.child(currentUser.getUid()).getChildren()) {
                    if (!ds.child("requestType").getValue(StateUser.REQUEST_TYPE.class).equals(StateUser.REQUEST_TYPE.friend)) {
                        final Request request = new Request();
                        request.setRequestType(ds.child("requestType").getValue(StateUser.REQUEST_TYPE.class));
                        request.setUID(ds.getKey());

                        dataSnapshot.getRef().getParent().child("users").child(request.getUID()).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                requests.clear();

                                request.setLogo(dataSnapshot.child("thumb").getValue().toString());
                                request.setStatusNet(dataSnapshot.child("statusNet").getValue(StatusUser.StatusNet.class));
                                request.setName(dataSnapshot.child("name").getValue().toString());
                                mapRequest.put(request.getUID(), request);

                                requests.addAll((Collection<? extends Request>) mapRequest.values());
                                adapterRequest.notifyDataSetChanged();
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        Log.d("requestFragment", ds.getKey());
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
