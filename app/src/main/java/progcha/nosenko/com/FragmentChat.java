package progcha.nosenko.com;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import adapter.AdapterLastMes;
import helper.Helper;
import helper.StatusUser;
import helper.Time;
import model.Chat;
import model.LastMessageChat;
import model.LastMessageGroupChat;
import model.SelectUser;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentChat extends Fragment {
    private RecyclerView recyclerView;
    private DatabaseReference reference, referenceUsers, messages, refRut;
    private View view;
    private FirebaseUser currentUser;
    private List<LastMessageChat> lastMessagesChats;
    private AdapterLastMes adapter;
    private Thread threadCountRec;
    private boolean isComplete = false;
    private DatabaseReference chats;
    private DatabaseReference refGroupChat;
    private List<LastMessageGroupChat> lastMessagesGroupChats;

    public FragmentChat() {
        // Required empty public constructor
    }

    @Override
    public void onStop() {
        super.onStop();

    }

    private class ViewHolderBorder extends RecyclerView.ViewHolder {
        public ViewHolderBorder(View itemView) {
            super(itemView);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_chat, container, false);

        recyclerView = view.findViewById(R.id.rec);
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference().child("users").child(currentUser.getUid()).child("friends");
        referenceUsers = FirebaseDatabase.getInstance().getReference().child("users");
        messages = referenceUsers.getParent().child("messages").child(currentUser.getUid());
        chats = referenceUsers.getParent().child("chat").child(currentUser.getUid());
        refRut = FirebaseDatabase.getInstance().getReference();
        refGroupChat = refRut.child("ChatGroup").child(currentUser.getUid());

        lastMessagesChats = new ArrayList<>();
        lastMessagesGroupChats = new ArrayList<>();

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new AdapterLastMes(lastMessagesChats, getContext());
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getLastMessagesChat();
        getLastMessageGroupChat();

        //getLastMessagesChat2();
        Helper.setStatusNet(StatusUser.StatusNet.ONLINE);
    }

    private void getLastMessagesChat() {
        chats.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //lastMessagesChats.clear();

                final Map mapCharts = new HashMap();

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    if (dataSnapshot.hasChild(ds.getKey())) {
                        final LastMessageChat lastMessageChat = new LastMessageChat();
                        lastMessageChat.setFriendUID(ds.getKey());

                        // Log.d("getLastMessagesChat", ds.toString());
                        if (ds.hasChild("message")) {
                            lastMessageChat.setMessage(ds.child("message").getValue().toString());
                            lastMessageChat.setTime(ds.child("timestamp").getValue().toString());
                            lastMessageChat.setSeen(ds.child("seen").getValue(Boolean.class));

                            //   Log.d("getLastMessagesChat", ds.child("state").getValue().toString());

                            dataSnapshot.getRef().getParent().getParent().child("users").child(lastMessageChat.getFriendUID()).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                   // lastMessagesChats.clear();

                                    adapter.clearChat();
                                    //getLastMessageGroupChat();

                                    lastMessageChat.setLogo(dataSnapshot.child("thumb").getValue().toString());
                                    lastMessageChat.setStatusNet(dataSnapshot.child("statusNet").getValue(StatusUser.StatusNet.class));
                                    lastMessageChat.setName(dataSnapshot.child("name").getValue().toString());
                                    mapCharts.put(lastMessageChat.getFriendUID(), lastMessageChat);

                                    lastMessagesChats.addAll((Collection<? extends LastMessageChat>) mapCharts.values());
                                    adapter.notifyDataSetChanged();
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                    if (databaseError != null) {

                                    }
                                }
                            });

                        }
//                    lastMessageChat.setNameChat(map.get("name").toString());
//                    lastMessageChat.setLogo(map.get("thumb").toString());
//                    lastMessageChat.setStatusNet((StatusUser.StatusNet) map.get("statusNet"));
                        // lastMessagesChats.add(lastMessageChat);


//                    DataSnapshot lastMessage = null;
//
//                    Map map = getLatItem(ds);
//                    lastMessage = (DataSnapshot) map.get("lastSnap");
//                    lastMessageChat.setMessage(lastMessage.child("message").getValue().toString());
//                    lastMessageChat.setTime(lastMessage.child("time").getValue().toString());
//
//                    dataSnapshot.getRef().getParent().getParent().child("users").child(lastMessageChat.getFriendUID()).addValueEventListener(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                            lastMessagesChats.clear();
//                            lastMessageChat.setLogo(dataSnapshot.child("thumb").getValue().toString());
//                            lastMessageChat.setStatusNet(dataSnapshot.child("statusNet").getValue(StatusUser.StatusNet.class));
//                            lastMessageChat.setNameChat(dataSnapshot.child("name").getValue().toString());
//                            lastMessagesChats.add(lastMessageChat);
//                            adapter.notifyDataSetChanged();
//                        }
//
//                        @Override
//                        public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                        }
//                    });
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void getLastMessageGroupChat() {
        refGroupChat.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                adapter.clearGroupChat();
                Map mapGroupChat = new HashMap();

                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    if (dataSnapshot.hasChild(ds.getKey())) {
                        final LastMessageChat lastMessageGroupChat = new LastMessageChat();
                        lastMessageGroupChat.setFriendUID(dataSnapshot.getKey());
                        lastMessageGroupChat.setGroupChatUID(ds.getKey());
//                        lastMessageGroupChat.setThumb(ds.child("thumb").getValue().toString());
                        Log.d("getLastMessageGroupChat", "  " + ds.child("users").getChildrenCount());
                        Map users = new HashMap();

                        lastMessageGroupChat.setCountUsers((int) ds.child("users").getChildrenCount());
                        lastMessageGroupChat.setName(ds.child("nameChat").getValue().toString());

                        for (DataSnapshot d : ds.child("users").getChildren())
                            users.put(d.getKey(), SelectUser.STATE_USER_GROUP_CHAT.added);
                        // Log.d("getLastMessageGroupChat", d.getKey());

                        lastMessageGroupChat.setUsers(users);
                        lastMessageGroupChat.setLastMessage(ds.child("lastMessage").getValue().toString());
                        lastMessageGroupChat.setNameUserLast(ds.child("nameUserLast").getValue().toString());
                        lastMessageGroupChat.setTime(ds.child("timestamp").getValue().toString());
                        lastMessageGroupChat.setSeen(ds.child("seen").getValue(Boolean.class));

                        mapGroupChat.put(lastMessageGroupChat.getGroupChatUID(), lastMessageGroupChat);

                        //lastMessagesGroupChats.addAll((Collection<? extends LastMessageGroupChat>) mapGroupChat.values());
                        lastMessagesChats.addAll((Collection<? extends LastMessageChat>) mapGroupChat.values());
                        // lastMessagesChats.add(lastMessageGroupChat);
                        adapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getLastMessagesChat2() {
        final Map mapAtrUser = new HashMap();

        chats.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lastMessagesChats.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    final LastMessageChat lastMessageChat = new LastMessageChat();
                    lastMessageChat.setFriendUID(ds.getKey());

                    Log.d("getLastMessagesChat2", dataSnapshot.child(lastMessageChat.getFriendUID()).getValue().toString());

                    lastMessageChat.setMessage(dataSnapshot.child(lastMessageChat.getFriendUID()).child("message").getValue().toString());
                    lastMessageChat.setTime(dataSnapshot.child(lastMessageChat.getFriendUID()).child("timestamp").getValue().toString());
                    lastMessageChat.setSeen(dataSnapshot.child(lastMessageChat.getFriendUID()).child("seen").getValue(Boolean.class));

                    dataSnapshot.getRef().getParent().getParent().child("users").child(lastMessageChat.getFriendUID()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            lastMessageChat.setLogo(dataSnapshot.child("thumb").getValue().toString());
                            lastMessageChat.setStatusNet(dataSnapshot.child("statusNet").getValue(StatusUser.StatusNet.class));
                            lastMessageChat.setName(dataSnapshot.child("name").getValue().toString());

                            mapAtrUser.put(lastMessageChat.getFriendUID(), lastMessageChat);
                            Log.d("sizeMap", mapAtrUser.size() + "");

                            lastMessagesChats.clear();
                            lastMessagesChats.addAll(mapAtrUser.values());
                            adapter.notifyDataSetChanged();
                            //  mapAtrUser.put(dataSnapshot.getKey() + "statusNet", dataSnapshot.child("statusNet").getValue(StatusUser.StatusNet.class));
                            //mapAtrUser.put(dataSnapshot.getKey() + "name", dataSnapshot.child("name").getValue().toString());
//                            lastMessageChat.setLogo(dataSnapshot.child("thumb").getValue().toString());
//                            lastMessageChat.setStatusNet(dataSnapshot.child("statusNet").getValue(StatusUser.StatusNet.class));
//                            lastMessageChat.setNameChat(dataSnapshot.child("name").getValue().toString());
//                            lastMessagesChats.add(lastMessageChat);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

//                    lastMessageChat.setLogo(mapAtrUser.get(lastMessageChat.getFriendUID()+"thumb").toString());
//                    lastMessageChat.setStatusNet((StatusUser.StatusNet) mapAtrUser.get(lastMessageChat.getFriendUID()+"statusNet"));
//                    lastMessageChat.setNameChat(mapAtrUser.get(lastMessageChat.getFriendUID()+"name").toString());


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private Map getLatItem(DataSnapshot ds) {
        Map map = new HashMap();
        for (DataSnapshot d : ds.getChildren()) {
            map.put("pushId", d.getKey());
            map.put("lastSnap", d);
        }

        return map;
    }
}
