package progcha.nosenko.com;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import adapter.AdapterFriends;
import helper.StatusUser;
import model.User;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentFrieds extends Fragment {


    private RecyclerView recyclerView;
    private FirebaseAuth firebaseAuth;
    private DatabaseReference reference, referenceUsers;
    private View view;
    private FirebaseUser firebaseUser;
    private List<User> friends;
    private List<String> friedsId;
    private AdapterFriends adapter;

    public FragmentFrieds() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_frieds, container, false);

        recyclerView = view.findViewById(R.id.rec);
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference().child("users").child(firebaseUser.getUid()).child("friends");
        referenceUsers = FirebaseDatabase.getInstance().getReference().child("users");

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        friends = new ArrayList<>();
        friedsId = new ArrayList<>();
        getFriends();

        return view;
    }

    private void getFriendsUID()
    {

    }

    private void getFriends()
    {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    //  User friend = new User();
                    // friend.setUID(ds.getValue().toString());
                    friedsId.add(ds.getKey());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        referenceUsers.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                friends.clear();
                for (DataSnapshot ds : dataSnapshot.getChildren()) {

                    if (friedsId.contains(ds.getKey())) {
                        Log.d("users", " " + ds.child("name").getValue());
                        User friend = new User();
                        friend.setUID(ds.getKey());
                        friend.setName(ds.child("name").getValue().toString());
                        friend.setIdLogo(ds.child("idLogo").getValue().toString());
                        friend.setStatusNet(ds.child("statusNet").getValue(StatusUser.StatusNet.class));
                        friend.setThumb(ds.child("thumb").getValue().toString());

                        friends.add(friend);
                    }
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter = new  AdapterFriends(friends, getContext());
        recyclerView.setAdapter(adapter);
    }


}
