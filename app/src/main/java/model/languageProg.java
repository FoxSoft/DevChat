package model;

import java.util.List;

public class languageProg {
    private String name;
    private int ageLerne;
    private String willTeach;

    public languageProg() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAgeLerne() {
        return ageLerne;
    }

    public void setAgeLerne(int ageLerne) {
        this.ageLerne = ageLerne;
    }

    public String getWillTeach() {
        return willTeach;
    }

    public void setWillTeach(String willTeach) {
        this.willTeach = willTeach;
    }
}
