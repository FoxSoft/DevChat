package model;

import helper.StatusUser;

public class Request {
    private String name;
    private String UID;
    private String logo;
    private StateUser.REQUEST_TYPE requestType;
    private StatusUser.StatusNet statusNet;

    public Request() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public StateUser.REQUEST_TYPE getRequestType() {
        return requestType;
    }

    public void setRequestType(StateUser.REQUEST_TYPE requestType) {
        this.requestType = requestType;
    }

    public StatusUser.StatusNet getStatusNet() {
        return statusNet;
    }

    public void setStatusNet(StatusUser.StatusNet statusNet) {
        this.statusNet = statusNet;
    }

    @Override
    public String toString() {
        return "Request{" +
                "name='" + name + '\'' +
                ", UID='" + UID + '\'' +
                ", logo='" + logo + '\'' +
                ", requestType=" + requestType +
                ", statusNet=" + statusNet +
                '}';
    }
}
