package model;

import java.util.Map;

public class GroupChat {
    private String nameChat;
    private int countUsers;
    private Map<String, SelectUser.STATE_USER_GROUP_CHAT> users;
    private String lastMessage;
    private String authorGroup;
    private Boolean seen = false;
    private Chat.STATE state = Chat.STATE.none;
    private String timestamp;
    private String thumb;
    private String nameUserLast;
    private String nameUser;

    public String getNameUserLast() {
        return nameUserLast;
    }

    public void setNameUserLast(String nameUserLast) {
        this.nameUserLast = nameUserLast;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public void setAuthorGroup(String authorGroup) {
        this.authorGroup = authorGroup;
    }

    public Boolean getSeen() {
        return seen;
    }

    public void setSeen(Boolean seen) {
        this.seen = seen;
    }

    public Chat.STATE getState() {
        return state;
    }

    public void setState(Chat.STATE state) {
        this.state = state;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getAuthorGroup() {
        return authorGroup;
    }

    public void setAuthorGroupUID(String authorGroup) {
        this.authorGroup = authorGroup;
    }

    public String getNameChat() {
        return nameChat;
    }

    public void setNameChat(String nameChat) {
        this.nameChat = nameChat;
    }

    public int getCountUsers() {
        return countUsers;
    }

    public void setCountUsers(int countUsers) {
        this.countUsers = countUsers;
    }

    public Map<String, SelectUser.STATE_USER_GROUP_CHAT> getUsers() {
        return users;
    }

    public void setUsers(Map<String, SelectUser.STATE_USER_GROUP_CHAT> users) {
        this.users = users;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getNameUser() {
        return nameUser;
    }
}
