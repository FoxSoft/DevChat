package model;

public class Notifications {
    public static enum TYPE_NOTIFI {
        REQUEST, MESSAGE, NEWS
    }

    private String from;
    private String to;
    private TYPE_NOTIFI type;
    private String message;
    private String title;

    public Notifications(String from, String to, TYPE_NOTIFI type, String message, String title) {
        this.from = from;
        this.to = to;
        this.type = type;
        this.message = message;
        this.title = title;
    }

    public Notifications() {
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public TYPE_NOTIFI getType() {
        return type;
    }

    public void setType(TYPE_NOTIFI type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
