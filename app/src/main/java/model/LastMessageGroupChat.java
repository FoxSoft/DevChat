package model;

import java.util.Map;

import helper.StatusUser;

public class LastMessageGroupChat {
    private String lastMessage;
    private Chat.TYPE_MESSAGE type;
    private boolean seen;
    private String time;
    private String fromLastMessage;
    private String logo;
    private StatusUser.StatusNet statusNet;
    private String name;
    private String friendUID;
    private int countUsers;
    private String groupChatUID;
    private String thumb;
    private Map<String, SelectUser.STATE_USER_GROUP_CHAT> users;

    public Map<String, SelectUser.STATE_USER_GROUP_CHAT> getUsers() {
        return users;
    }

    public void setUsers(Map<String, SelectUser.STATE_USER_GROUP_CHAT> users) {
        this.users = users;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getGroupChatUID() {
        return groupChatUID;
    }

    public void setGroupChatUID(String groupChatUID) {
        this.groupChatUID = groupChatUID;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public Chat.TYPE_MESSAGE getType() {
        return type;
    }

    public void setType(Chat.TYPE_MESSAGE type) {
        this.type = type;
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFromLastMessage() {
        return fromLastMessage;
    }

    public void setFromLastMessage(String fromLastMessage) {
        this.fromLastMessage = fromLastMessage;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public StatusUser.StatusNet getStatusNet() {
        return statusNet;
    }

    public void setStatusNet(StatusUser.StatusNet statusNet) {
        this.statusNet = statusNet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFriendUID() {
        return friendUID;
    }

    public void setFriendUID(String friendUID) {
        this.friendUID = friendUID;
    }

    public int getCountUsers() {
        return countUsers;
    }

    public void setCountUsers(int countUsers) {
        this.countUsers = countUsers;
    }
}
