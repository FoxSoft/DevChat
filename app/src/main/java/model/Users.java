package model;

import helper.StatusUser;

public class Users {
    private String name;
    private String idLogo;
    private StatusUser.StatusNet statusNet;
    private String thumb;
    private String uid;

    public Users(String name, String idLogo, StatusUser.StatusNet statusNet, String thumb, String uid) {
        this.name = name;
        this.idLogo = idLogo;
        this.statusNet = statusNet;
        this.thumb = thumb;
        this.uid = uid;

    }

    public Users() {
    }

    public String getThumb() {
        return thumb;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StatusUser.StatusNet getStatusNet() {
        return statusNet;
    }

    public void setStatusNet(StatusUser.StatusNet statusNet) {
        this.statusNet = statusNet;
    }

    public String getIdLogo() {
        return idLogo;
    }

    public void setIdLogo(String idLogo) {
        this.idLogo = idLogo;
    }

    @Override
    public String toString() {
        return "Users{" +
                "name='" + name + '\'' +
                ", idLogo='" + idLogo + '\'' +
                '}';
    }
}
