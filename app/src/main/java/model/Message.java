package model;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;

public class Message {
    private String message;
    private String date;
    private int messageCount;
    private String author;
    private Calendar calendar;
    private String title;

    public Message(String message, String date, String author, String title) {
        calendar = Calendar.getInstance();
        this.message = message;
        this.date = ""+calendar.getTime().getHours()+" ч."+calendar.getTime().getMinutes()+" мин.";
        this.author = author;
        this.title = title;
    }

    public Message() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getMessageCount() {
        return messageCount;
    }

    public void setMessageCount(int messageCount) {
        this.messageCount = messageCount;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
