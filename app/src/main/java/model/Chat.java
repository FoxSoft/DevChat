package model;

import java.util.Calendar;

public class Chat {
    public static enum TYPE_MESSAGE{
        text, audio, video, picture
    }
    public static enum  STATE{
        write, none
    }

    private String message;
    private TYPE_MESSAGE type;
    private boolean seend;
    private String time;
    private String from;
    public boolean seen;

    public Chat() {
    }

    public boolean isSeen() {
        return seen;
    }

    public void setSeen(boolean seen) {
        this.seen = seen;
    }

    public String getMessage() {
        return message;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public TYPE_MESSAGE getType() {
        return type;
    }

    public void setType(TYPE_MESSAGE type) {
        this.type = type;
    }

    public boolean isSeend() {
        return seend;
    }

    public void setSeend(boolean seend) {
        this.seend = seend;
    }

    public String getTime() {
        return time;
    }

    public void setTimeNow() {
        Calendar calendar = Calendar.getInstance();
        this.time = ""+calendar.getTime().getHours()+" ч."+calendar.getTime().getMinutes()+" мин.";
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Chat{" +
                "message='" + message + '\'' +
                ", type=" + type +
                ", seend=" + seend +
                ", time='" + time + '\'' +
                ", from='" + from + '\'' +
                '}';
    }
}
