package model;

import java.util.List;

import helper.StatusUser;

public class User {
    private String name;
    private String number;
    private String email;
    private String status;
    private String UID;
    private String idLogo;
    private String dateOfBirth;
    private gender gender;
    private List<languageProg> languageProg;
    private String age;
    private StatusUser.StatusNet statusNet;
    private String thumb;
    private String friends;
    private int countFriends;
    private String tokenId;
    private String lastSeen;

    public User(String name, String number, String email , String status, String UID, String idLogo,
                StatusUser.StatusNet statusNet, String thumb, String friends, String tokenId, String lastSeen) {
        this.name = name;
        this.number = number;
        this.email = email;

        this.status = status;
        this.UID = UID;
        this.idLogo = idLogo;
        this.statusNet = statusNet;
        this.thumb = thumb;
        this.friends = friends;
        this.tokenId = tokenId;
        this.lastSeen = lastSeen;
    }

    public User(String name, String status, String UID) {
        this.name = name;
        this.status = status;
        this.UID = UID;
    }

    public int getCountFriends() {
        return countFriends;
    }

    public void setCountFriends(int countFriends) {
        this.countFriends = countFriends;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getFriends() {
        return friends;
    }

    public void setFriends(String friends) {
        this.friends = friends;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public User() {
    }

    public StatusUser.StatusNet getStatusNet() {
        return statusNet;
    }

    public void setStatusNet(StatusUser.StatusNet statusNet) {
        this.statusNet = statusNet;
    }

    public String getGender() {
        return gender.value;
    }

    public gender getGenderV2()
    {
        return this.gender;
    }

    public void setGender(User.gender gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public List<model.languageProg> getLanguageProg() {
        return languageProg;
    }

    public void setLanguageProg(List<model.languageProg> languageProg) {
        this.languageProg = languageProg;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public String getIdLogo() {
        return idLogo;
    }

    public void setIdLogo(String idLogo) {
        this.idLogo = idLogo;
    }

    public static enum gender{
        wooman("жен"), man("муж");


        private String value;
        gender(String value) {
            this.value = value;
        }
    }
}
