package model;

public class SelectUser {
    private String name;
    private String UID;
    private boolean check;
    private STATE_USER_GROUP_CHAT state;

    public enum STATE_USER_GROUP_CHAT{
        added, expects, request, denied;
    }

    public SelectUser(String name, String UID, boolean check, STATE_USER_GROUP_CHAT state) {
        this.name = name;
        this.UID = UID;
        this.check = check;
        this.state = state;
    }

    public SelectUser() {
    }

    public STATE_USER_GROUP_CHAT getState() {
        return state;
    }

    public void setState(STATE_USER_GROUP_CHAT state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUID() {
        return UID;
    }

    public void setUID(String UID) {
        this.UID = UID;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
