package servises;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.widget.ImageView;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import progcha.nosenko.com.ActivityChat;
import progcha.nosenko.com.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class ServiseFireBaseMessages extends FirebaseMessagingService {
    private static final String CHANNEL_REQUEST_ID = "req_dev_chat";
    private NotificationManager notificationManager;
    final static private String CHANNEL_MESSAGE_ID = "mes_dev_chat";
    private Notification.Builder builder26API;
    private NotificationChannel channelRequest;


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (remoteMessage.getData() != null) {
            sendNotification(remoteMessage);
//            Map<String, String> data = remoteMessage.getData();
//
//            String title = data.get("title");
//            String body = data.get("body");
//            String tag = remoteMessage.getNotification().getTag();
//            String clickAction = remoteMessage.getNotification().getClickAction();
//            String UID = data.get("UID");
//            String name = data.get("name");
//            String thumb = data.get("thumb");
//
//            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
//
//            if (tag.equals("request")) {
//                Intent intent = new Intent(clickAction);
//                intent.putExtra("UID", UID);
//
//                Bitmap bitmap = getBitmapFromURL(thumb);
//
//                builder.setSmallIcon(R.drawable.icon_notify)
//                        .setContentTitle(title)
//                        .setContentText(remoteMessage.getNotification().getBody())
//                        .setLargeIcon(bitmap)
//                        .addAction(R.drawable.friend_but, "добавить", null)
//                        .addAction(R.drawable.no_friend_but, "отклонить", PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), PendingIntent.FLAG_CANCEL_CURRENT))
//                        .setContentIntent(createIntent(intent))
//                        .setAutoCancel(true);
//                builder.getNotification().ledARGB = Color.parseColor("#FF033E");
//                builder.getNotification().ledOffMS = 300;
//                builder.getNotification().ledOnMS = 1000;
//                builder.getNotification().flags = builder.getNotification().flags | Notification.FLAG_SHOW_LIGHTS;
//            }
//
//            if (tag.equals("message")) {
//
//                Bitmap bitmap = getBitmapFromURL(thumb);
//                Intent intent = new Intent(getApplicationContext(), ActivityChat.class);
//                intent.putExtra("name", name);
//                intent.putExtra("UID", UID);
//                intent.putExtra("thumb", thumb);
//
//                intent.getExtras();
//                builder.setSmallIcon(R.drawable.icon_notify)
//                        .setContentTitle(title)
//                        .setContentText(remoteMessage.getNotification().getBody())
//                        .setLargeIcon(bitmap)
//                        .setContentIntent(createIntent(intent))
//                        .setAutoCancel(true);
//                builder.getNotification().ledARGB = Color.parseColor("#FF033E");
//                builder.getNotification().ledOffMS = 300;
//                builder.getNotification().ledOnMS = 1000;
//                builder.getNotification().flags = builder.getNotification().flags | Notification.FLAG_SHOW_LIGHTS;
//            }
//
//
//            notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//            int notifyID = (int) System.currentTimeMillis();
//            notificationManager.notify(notifyID, builder.build());
        }
    }


    private PendingIntent createIntent(Intent intent) {
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return pendingIntent;
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            if (src != null || !src.equals("")) {
                URL url = new URL(src);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;
            } else {
                Resources resources = Resources.getSystem();
                Bitmap myBitmap = BitmapFactory.decodeResource(resources, R.drawable.default_avatar);
                return myBitmap;
            }
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }

    private void sendNotification(RemoteMessage remoteMessage) {
        Map<String, String> data = remoteMessage.getData();

        String title = data.get("title");
        String body = data.get("body");
        String tag = remoteMessage.getNotification().getTag();
        String clickAction = remoteMessage.getNotification().getClickAction();
        String UID = data.get("UID");
        String name = data.get("name");
        String thumb = data.get("thumb");

        int notifyID = (int) System.currentTimeMillis();
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            if (tag.equals("message")) {
                builder26API = new Notification.Builder(getApplicationContext(), CHANNEL_MESSAGE_ID);
                NotificationChannel channelMessages = new NotificationChannel(CHANNEL_MESSAGE_ID, "DevChat Mes Channel", NotificationManager.IMPORTANCE_DEFAULT);

                channelMessages.setDescription("DevChat Messages Channel");
                channelMessages.enableLights(true);
                channelMessages.setLightColor(Color.MAGENTA);
                channelMessages.enableVibration(true);
                channelMessages.setVibrationPattern(new long[]{0, 1000, 500, 1000, 5000});

                Bitmap bitmap = getBitmapFromURL(thumb);
                Intent intent = new Intent(getApplicationContext(), ActivityChat.class);
                intent.putExtra("name", name);
                intent.putExtra("UID", UID);
                intent.putExtra("thumb", thumb);

                builder26API.setSmallIcon(R.drawable.icon_notify)
                        .setContentTitle(title)
                        .setLargeIcon(bitmap)
                        .setContentIntent(createIntent(intent))
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setDefaults(Notification.DEFAULT_LIGHTS);

                notificationManager.createNotificationChannel(channelMessages);
            }

            if (tag.equals("request")) {
                builder26API = new Notification.Builder(getApplicationContext(), CHANNEL_REQUEST_ID);

                NotificationChannel channelRequest = new NotificationChannel(CHANNEL_REQUEST_ID, "DevChat Mes Channel", NotificationManager.IMPORTANCE_DEFAULT);

                channelRequest.setDescription("DevChat Request Channel");
                channelRequest.enableLights(true);
                channelRequest.setLightColor(Color.MAGENTA);
                channelRequest.enableVibration(true);
                channelRequest.setVibrationPattern(new long[]{0, 1000, 500, 1000, 5000});

                Intent intent = new Intent(clickAction);
                intent.putExtra("UID", UID);

                Bitmap bitmap = getBitmapFromURL(thumb);

                builder26API.setSmallIcon(R.drawable.icon_notify)
                        .setContentTitle(title)
                        .setLargeIcon(bitmap)
                        .setContentIntent(createIntent(intent))
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setDefaults(Notification.DEFAULT_LIGHTS);

                notificationManager.createNotificationChannel(channelRequest);
            }

            notificationManager.notify(notifyID, builder26API.build());

        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext());

            if (tag.equals("message")) {
                Bitmap bitmap = getBitmapFromURL(thumb);
                Intent intent = new Intent(getApplicationContext(), ActivityChat.class);
                intent.putExtra("name", name);
                intent.putExtra("UID", UID);
                intent.putExtra("thumb", thumb);

                intent.getExtras();
                builder.setSmallIcon(R.drawable.icon_notify)
                        .setContentTitle(title)
                        .setContentText(remoteMessage.getNotification().getBody())
                        .setLargeIcon(bitmap)
                        .setContentIntent(createIntent(intent))
                        .setLights(Color.MAGENTA, 1000, 500)
                        .setAutoCancel(true);
                builder.getNotification().ledARGB = Color.MAGENTA;
                builder.getNotification().ledOffMS = 500;
                builder.getNotification().ledOnMS = 1000;
                builder.getNotification().flags = builder.getNotification().flags | Notification.FLAG_SHOW_LIGHTS;
            }

            if (tag.equals("request")) {
                Intent intent = new Intent(clickAction);
                intent.putExtra("UID", UID);

                Bitmap bitmap = getBitmapFromURL(thumb);

                builder.setSmallIcon(R.drawable.icon_notify)
                        .setContentTitle(title)
                        .setContentText(remoteMessage.getNotification().getBody())
                        .setLargeIcon(bitmap)
                        .addAction(R.drawable.friend_but, "добавить", createIntent(intent))
                        .addAction(R.drawable.no_friend_but, "отклонить", PendingIntent.getActivity(getApplicationContext(), 0, new Intent(), PendingIntent.FLAG_CANCEL_CURRENT))
                        .setContentIntent(createIntent(intent))
                        .setLights(Color.MAGENTA, 1000, 500)
                        .setAutoCancel(true);
                builder.getNotification().ledARGB = Color.MAGENTA;
                builder.getNotification().ledOffMS = 500;
                builder.getNotification().ledOnMS = 1000;
                builder.getNotification().flags = builder.getNotification().flags | Notification.FLAG_SHOW_LIGHTS;
            }

            notificationManager.notify(notifyID, builder.build());
        }


    }
}
